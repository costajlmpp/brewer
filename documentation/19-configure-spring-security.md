# Configure Basic Spring Security

## Dependencies

```xml
        <!-- Spring Security -->
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
            <version>${spring-security.version}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
            <version>${spring-security.version}</version>
            <scope>compile</scope>
        </dependency>

```

## Initializer

Extends AbstractSecurityWebApplicationInitializer.

This class initializes spring security, with the application.
Spring security works with HTTP filters (Servlet Filters), this class has a responsibility to start.
It is also possible to customize this stake through Override methods.


```java
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
```

## Configurator

Extends WebSecurityConfigurerAdapter and @EnableWebSecurity

```java


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfigurator extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("admin")
                .roles("CREATE_CLIENT");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // All the requisitions must be authenticated
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                // necessary to enable the logout request
                .csrf().disable();
    }
}
```

## URL

* GET: /logout
* GET: /login
