drop table if exists user_group_permission;
drop table if exists user_group_user;
drop table if exists permission;
drop table if exists user_group;
drop table if exists user;


CREATE TABLE user (
  id             BIGINT (20) PRIMARY KEY AUTO_INCREMENT,
  name           VARCHAR(500) NOT NULL,
  email          VARCHAR(150) CHARACTER SET UTF8MB3 COLLATE utf8_bin NOT NULL UNIQUE,
  password       VARCHAR(1024) NOT NULL,
  enable BOOLEAN NOT NULL DEFAULT true

) ENGINE = InnoDB DEFAULT CHARSET = UTF8MB4;

CREATE TABLE user_group (
    id    BIGINT (20) PRIMARY KEY AUTO_INCREMENT,
    name  VARCHAR(100) CHARACTER SET  UTF8MB3 COLLATE utf8_bin NOT NULL UNIQUE

) ENGINE = InnoDB DEFAULT CHARSET = UTF8MB4;

CREATE TABLE permission (
    id    BIGINT (20) PRIMARY KEY AUTO_INCREMENT,
    name  VARCHAR(100) CHARACTER SET  UTF8MB3 COLLATE utf8_bin NOT NULL UNIQUE

) ENGINE = InnoDB DEFAULT CHARSET = UTF8MB4;

CREATE TABLE user_group_user (
    user_id       BIGINT(20) NOT NULL,
    user_group_id BIGINT(20) NOT NULL,
    PRIMARY KEY (user_id, user_group_id),
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (user_group_id) REFERENCES user_group(id)
) ENGINE=InnoDB DEFAULT CHARSET= UTF8MB3;

CREATE TABLE user_group_permission (
    user_group_id   BIGINT(20) NOT NULL,
    permission_id   BIGINT(20) NOT NULL,
    PRIMARY KEY (user_group_id, permission_id),
    FOREIGN KEY (user_group_id) REFERENCES user_group(id),
    FOREIGN KEY (permission_id) REFERENCES permission(id)
) ENGINE=InnoDB DEFAULT CHARSET= UTF8MB3;