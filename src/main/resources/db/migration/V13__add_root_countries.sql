INSERT INTO country (id, name, acronym) VALUES (1, 'Portugal', 'PT');
INSERT INTO country (id, name, acronym) VALUES (2, 'Spain', 'ES');
INSERT INTO country (id, name, acronym) VALUES (3, 'France', 'FR');
INSERT INTO country (id, name, acronym) VALUES (4, 'German', 'GR');


insert into city (id, name, country_id)
values
(1, 'Lisbon', 1),
(2, 'Coimbra', 1),
(3, 'Soure', 1),
(4, 'Pouca Pena', 1),
(5, 'Paris', 3),
(6, 'Madrid', 2),
(7, 'Vigo', 2),
(8, 'Munich', 4),
(9, 'Humburgo', 4)
;
