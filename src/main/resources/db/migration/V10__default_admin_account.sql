INSERT INTO permission (id, name)
VALUES (1, 'MANAGER_CITIES'),
       (2, 'MANAGER_USERS');


/*
INSERT INTO user_group (id, name)
VALUES
(1, 'Administrator'),
(2, 'Vendor');
*/

INSERT INTO user_group_permission (user_group_id, permission_id)
VALUES (1, 1),
       (1, 2);

INSERT INTO user (id, name, email, password, enable, assigned_at)
VALUES (1, 'Administrator', 'admin@brewer.moc', '$2a$10$g.wT4R0Wnfel1jc/k84OXuwZE02BlACSLfWy6TycGPvvEKvIm86SG', 1,
        '2019-03-17'),
       (2, 'Jonas', 'jonas@brewer.moc', '$2a$10$g.wT4R0Wnfel1jc/k84OXuwZE02BlACSLfWy6TycGPvvEKvIm86SG', 1,
        '2019-03-17'),
       (4, 'Vicente', 'vicente@mail.moc', '$2a$10$g.wT4R0Wnfel1jc/k84OXuwZE02BlACSLfWy6TycGPvvEKvIm86SG', 1, null);

INSERT INTO user_group_user (user_id, user_group_id)
VALUES (1, 1),
       (2, 2);




