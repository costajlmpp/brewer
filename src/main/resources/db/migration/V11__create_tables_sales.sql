create table sale
(
    id        bigint auto_increment primary key,
    client_id bigint         not null,
    total     decimal(10, 2) not null default 0.0,
    state     smallint       not null default 0,
    version   int            not null default 0,
    create_at timestamp               default now(),

    constraint fk_sale_client__fk foreign key (client_id) references client (id)
)
    comment 'sales';

create index sale_client_id_index on sale (client_id);


create table sales_item
(
    sale_id    bigint        not null references sale (id),
    beer_id    bigint        not null references beer (id),
    unit_value decimal(10, 2),
    qty        int default 1 not null,

    primary key (sale_id, beer_id)

) comment 'sales items';

create index sales_item_sale_id_index on sales_item (sale_id);
create index sales_item_beer_id_index on sales_item (beer_id);


alter table sales_item add foreign key sales_item_beer__fk (beer_id) references beer(id);
alter table sales_item add foreign key sales_item_sale_id_fk (sale_id) references sale(id);

alter table sale drop foreign key fk_sale_client__fk;
alter table sale add constraint sale_client__fk foreign key (client_id) references client (id);


