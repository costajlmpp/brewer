CREATE TABLE client (
  id             BIGINT (20) PRIMARY KEY AUTO_INCREMENT,
  name           VARCHAR(500) NOT NULL,
  nif            INTEGER (9) NOT NULL,
  phone          VARCHAR(20),
  email          VARCHAR(150),
  city_id        BIGINT (20),
  street_address VARCHAR(1024),
  zip_code       VARCHAR(20),

  CONSTRAINT fk_client_city_id_of_city FOREIGN KEY (city_id) REFERENCES city (id),
  CONSTRAINT uk_client_nif_unique UNIQUE (nif)
) ENGINE = InnoDB DEFAULT CHARSET = UTF8MB4;
