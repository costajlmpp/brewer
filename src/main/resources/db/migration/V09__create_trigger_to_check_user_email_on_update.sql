delimiter |

create trigger user_email_to_lower_on_update
  before INSERT
  on user
  for each row
  BEGIN
    -- variable declarations
    -- trigger code

    SET NEW.email = LOWER(NEW.email);

  END

|
delimiter;

