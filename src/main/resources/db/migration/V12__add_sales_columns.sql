alter table sale add ports_value decimal(10,2) default 0 not null;
alter table sale add discount_value decimal(10,2) default 0 not null;
alter table sale add observations VARCHAR(1024);
alter table sale add delivery_date datetime;
alter table sale add user_id bigint not null;
alter table sale add constraint sale_user_id__fk foreign key (user_id) references user (id);

/*

delimiter '//'

CREATE PROCEDURE addcol() BEGIN

    IF NOT EXISTS(
            SELECT * FROM INFORMATION_SCHEMA.COLUMNS
            WHERE COLUMN_NAME='ports_value'
              AND TABLE_NAME='sale'
              AND TABLE_SCHEMA='brewerdb'
        ) THEN
        alter table sale add ports_value decimal(10,2) default 0 not null;
    END IF;

    IF NOT EXISTS(
            SELECT * FROM INFORMATION_SCHEMA.COLUMNS
            WHERE COLUMN_NAME='discount_value'
              AND TABLE_NAME='sale'
              AND TABLE_SCHEMA='brewerdb'
        ) THEN
        alter table sale add discount_value decimal(10,2) default 0 not null;
    END IF;

    IF NOT EXISTS(
            SELECT * FROM INFORMATION_SCHEMA.COLUMNS
            WHERE COLUMN_NAME='observations'
              AND TABLE_NAME='sale'
              AND TABLE_SCHEMA='brewerdb'
        ) THEN
        alter table sale add observations VARCHAR(1024);
    END IF;

    IF NOT EXISTS(
            SELECT * FROM INFORMATION_SCHEMA.COLUMNS
            WHERE COLUMN_NAME='delivery_date'
              AND TABLE_NAME='sale'
              AND TABLE_SCHEMA='brewerdb'
        ) THEN
        alter table sale add delivery_date datetime;
    END IF;

    IF NOT EXISTS(
            SELECT * FROM INFORMATION_SCHEMA.COLUMNS
            WHERE COLUMN_NAME='user_id'
              AND TABLE_NAME='sale'
              AND TABLE_SCHEMA='brewerdb'
        ) THEN
        alter table sale add user_id bigint not null;
        alter table sale add constraint sale_user_id__fk foreign key (user_id) references user (id);

    END IF;



END;
'//'
delimiter ';'

CALL addcol();

DROP PROCEDURE addcol;

 */