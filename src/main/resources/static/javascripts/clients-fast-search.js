var Brewer = Brewer || {};


Brewer.ClientsFastSearchTableResult = (function () {

    function ClientsFastSearchTableResult(modal) {
        this.modalCliente = modal;
        this.cliente = $('.js-table-row-fast-search-clients');
    }

    ClientsFastSearchTableResult.prototype.init = function () {
        this.cliente.on('click', onRowSelectedClick.bind(this));
    };

    function onRowSelectedClick(event) {
        this.modalCliente.modal('hide');

        var clienteSelecionado = $(event.currentTarget);
        $('#nameClient').val(clienteSelecionado.data('name'));
        $('#codigoCliente').val(clienteSelecionado.data('id'));
    }

    //
    //     var selectedRow = $(event.currentTarget);
    //
    //     var value = selectedRow.data('name');
    //     var value1 = selectedRow.data('id');
    //
    //     $('#nameClient').val(value);
    //     //$('#codigoCliente').val(value1);
    //     $('#codigoCliente').val(clienteSelecionado.data('codigo'));
    //
    //     this.modalCliente.modal('hide');
    // }

    return ClientsFastSearchTableResult;

}());


Brewer.ClientsFastSearch = (function () {

    function ClientsFastSearch() {
        this.fastClientsSearchModal = $('#fastClientsSearch');
        this.searchBtn = $('.js-clients-fast-search-btn');
        this.clientNameInput = $('#clientName');
        this.errorMessage = $('.js-error-message');
        this.containerTabelaPesquisa = $('#container-fast-search-results');
        this.htmlTabelaPesquisa = $('#fr-table-fast-search-clients').html();
        this.template = Handlebars.compile(this.htmlTabelaPesquisa);
    }

    ClientsFastSearch.prototype.initialize = function () {
        this.searchBtn.on('click', onSearchButtonClick.bind(this));
        this.fastClientsSearchModal.on('shown.bs.modal', onModalShow.bind(this));
    };

    function onModalShow() {
        this.clientNameInput.focus();
    }

    function onSearchButtonClick(event) {
        event.preventDefault();

        var url = this.fastClientsSearchModal.find('form').attr('action');

        $.ajax({
            url: url,
            method: 'GET',
            contentType: 'application/json',
            data: {
                name: this.clientNameInput.val()
            },
            success: onSearchFinishedHandler.bind(this),
            error: onErrorHandler.bind(this)
        });
    }

    function onSearchFinishedHandler(result) {
        this.errorMessage.addClass('hidden');
        //console.log('result:' + JSON.stringify(result))
        var html = this.template(result);
        this.containerTabelaPesquisa.html(html);

        var tableResult = new Brewer.ClientsFastSearchTableResult(this.fastClientsSearchModal);
        tableResult.init();
    }

    function onErrorHandler() {
        this.errorMessage.removeClass('hidden');
    }

    return ClientsFastSearch;

}());


$(function () {
    var clientsFastSearch = new Brewer.ClientsFastSearch();
    clientsFastSearch.initialize();
});