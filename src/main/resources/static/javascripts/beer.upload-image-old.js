$(function() {
    var settings = {
        action: '/brewer/images',
        type: 'json',
        filelimit: 1,
        allow: '*.(jpg|jpeg|png)',
        complete: function(response) {
            var beerImageName = $('.js_image_name');
            var beerImageContentType = $('.js_image_contentType');

            beerImageName.val(response.name);
            beerImageContentType.val(response.contentType);

            var htmlTemplateSource = $('#image-box').html();
            var template = Handlebars.compile(htmlTemplateSource);
            var compiledHtml = template({imageId: response.name});


            // now we should switch the js-beer-image content
            var uploadDrop = $('#upload-drop');
            uploadDrop.addClass('hidden');
            var beerImageContainer = $('.js-beer-image');
            beerImageContainer.append(compiledHtml);

            $('.js-remove-image-box').on('click', function() {
                $('.js-image-box').remove();
                uploadDrop.removeClass('hidden');

                beerImageName.val(null);
                beerImageContentType.val(null);
            });
        }
    };

    UIkit.uploadSelect($('#upload-select'), settings);
    UIkit.uploadDrop($('#upload-drop'), settings);
});
