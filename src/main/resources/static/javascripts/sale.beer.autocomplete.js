Brewer = Brewer || {};

Brewer.SaleBeerAutoComplete = (function () {

    function SaleBeerAutoComplete() {
        this.codeOrNameinput = $('.js-beer-code-or-name-input');
        var htmlTemplateAutocomplete = $('#template-sale-beer-auto-complete').html();
        this.template = Handlebars.compile(htmlTemplateAutocomplete);
        this.eventEmitter = $({});
        this.on = this.eventEmitter.on.bind(this.eventEmitter);
    }

    SaleBeerAutoComplete.prototype.initialize = function () {
        var url = this.codeOrNameinput.attr('url');

        var options = {
            url: function (codeOrName) {
                //return '/brewer/beers/summary?codeOrName=' + codeOrName;
                return url + codeOrName;
            },
            getValue: 'name',
            minCharNumber: 2,
            requestDelay: 300,
            ajaxSettings: {
                contentType: 'application/json'
            },
            template: {
                type: 'custom',
                method: parseToTemplate.bind(this)
            },
            list: {
                onChooseEvent: onSelectedItem.bind(this)
            }
        };

        this.codeOrNameinput.easyAutocomplete(options);
    };

    function onSelectedItem() {
        console.log('One item has been selected');

        var itemDate = this.codeOrNameinput.getSelectedItemData();
        console.log('sales-item-table-selected-item' + itemDate);

        this.eventEmitter.trigger('sales-item-table-selected-item', itemDate);
    }

    function parseToTemplate(name, beer) {
        //beer.formatedValue = Brewer.formatCurrency(beer.value);

        if (beer.imageName == null) {
            beer.imageName = 'beer-mock.png';
        }

        return this.template(beer);
    }

    return SaleBeerAutoComplete;

}());

/*
$(function () {
    var saleBeerAutoComplete = new Brewer.SaleBeerAutoComplete();
    saleBeerAutoComplete.initialize();
});
 */

