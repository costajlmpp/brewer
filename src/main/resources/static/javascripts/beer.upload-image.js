var Brewer = Brewer || {};

Brewer.UploadImage = (function () {

    function UploadImage() {
        this.inputImageName = $('.js_image_name');
        this.inputImageContentType = $('.js_image_contentType');

        this.htmlBeerImageTemplate = $('#image-box').html();
        this.template = Handlebars.compile(this.htmlBeerImageTemplate);

        this.containerBeerImage = $('.js-beer-image');

        this.uploadDrop = $('#upload-drop');
    }

    UploadImage.prototype.init = function () {
        var settings = {
            type: 'json',
            filelimit: 1,
            allow: '*.(jpg|jpeg|png)',
            //action: '/brewer/images',
            action: this.containerBeerImage.data('url-images'),
            complete: onUploadComplete.bind(this),
            beforeSend: addCsrfToken
        };

        UIkit.uploadSelect($('#upload-select'), settings);
        UIkit.uploadDrop(this.uploadDrop, settings);

        if (this.inputImageName.val()) {
            onUploadComplete.call(this,
                {
                    name: this.inputImageName.val(),
                    contentType: this.inputImageContentType.val()
                });
        }
    };

    function onUploadComplete(response) {
        this.inputImageName.val(response.name);
        this.inputImageContentType.val(response.contentType);

        this.uploadDrop.addClass('hidden');
        var htmlBeerImage = this.template({imageId: response.name});
        this.containerBeerImage.append(htmlBeerImage);

        $('.js-remove-image-box').on('click', onRemoveImage.bind(this));
    }

    function onRemoveImage() {
        $('.js-image-box').remove();
        this.uploadDrop.removeClass('hidden');
        this.inputImageName.val(null);
        this.inputImageContentType.val(null);
    }

    function addCsrfToken(xhr) {
        var token = $('input[name=_csrf]').val();
        var header = $('input[name=_csrf_header]').val();
        xhr.setRequestHeader(header, token);
    }

    return UploadImage;

})();

$(function () {
    var uploadImage = new Brewer.UploadImage();
    uploadImage.init();
});