var Brewer = Brewer || {};

Brewer.ComboCountry = (function () {

    function ComboCountry() {
        this.combo = $('#country');
        this.emitter = $({});
        this.on = this.emitter.on.bind(this.emitter);
    }

    ComboCountry.prototype.init = function () {
        this.combo.on('change', onCountryChanged.bind(this));
    }

    function onCountryChanged() {
        this.emitter.trigger('selected-country-change', this.combo.val());
    }

    return ComboCountry;

}());

Brewer.ComboCity = (function () {

    function ComboCity(comboCountry) {
        this.comboCountry = comboCountry;
        this.combo = $('#city');
        this.imgLoading = $('.js-img-loading');
        this.inputHiddenSelectedCity = $('#inputHiddenSelectedCity');
    }

    ComboCity.prototype.init = function () {
        reset.call(this);
        this.comboCountry.on('selected-country-change', onCountryChanged.bind(this));
        var countryId = this.comboCountry.combo.val();
        initializeCities.call(this, countryId);
    }

    function onCountryChanged(evento, countryId) {
        this.inputHiddenSelectedCity.val('');
        initializeCities.call(this, countryId);
    }

    function initializeCities(countryId) {
        if (countryId) {
            var response = $.ajax({
                url: this.combo.attr('url'), //this.combo.data('url'),
                method: 'GET',
                contentType: 'application/json',
                data: {'countryId': countryId},
                beforeSend: initRequisition.bind(this),
                complete: finalizeRequisition.bind(this)
            });
            response.done(onGetCitiesCompletes.bind(this));
        } else {
            reset.call(this);
        }
    }

    function onGetCitiesCompletes(cities) {
        var options = [];
        cities.forEach(function (city) {
            options.push('<option value="' + city.id + '">' + city.name + '</option>');
        });

        this.combo.html(options.join(''));
        this.combo.removeAttr('disabled');

        var selectedCityId = this.inputHiddenSelectedCity.val();
        if (selectedCityId) {
            this.combo.val(selectedCityId);
        }
    }

    function reset() {
        this.combo.html('<option value="">Select one city</option>');
        this.combo.val('');
        this.combo.attr('disabled', 'disabled');
    }

    function initRequisition() {
        reset.call(this);
        this.imgLoading.show();
    }

    function finalizeRequisition() {
        this.imgLoading.hide();
    }

    return ComboCity;

}());

$(function () {

    var comboCountry = new Brewer.ComboCountry();
    comboCountry.init();

    var comboCity = new Brewer.ComboCity(comboCountry);
    comboCity.init();

});