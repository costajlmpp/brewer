var Brewer = Brewer || {}

Brewer.FastStyleRegiste = (function () {

    function FastStyleRegiste() {
        this.modal = $("#style-fast-registration");
        this.saveButton = this.modal.find('.js-style-fast-registration-save-button');
        this.form = this.modal.find('form');
        this.actionUrl = this.form.attr('action');
        this.inputNametyle = $('#styleName');
        this.containerMessagesError = this.modal.find('.js-message-style-fast-registration');
    }

    FastStyleRegiste.prototype.initialize = function () {
        this.form.on('submit', function (event) {
            event.preventDefault()
        });
        this.modal.on('shown.bs.modal', onModalShow.bind(this));
        this.modal.on('hide.bs.modal', onModalClose.bind(this))
        this.saveButton.on('click', onSaveButtonClick.bind(this));
    }


    function onModalShow() {
        this.inputNametyle.focus();
    }

    function onModalClose() {
        this.inputNametyle.val('');
        this.containerMessagesError.addClass('hidden');
        this.form.find('.form-group').removeClass('has-error');
    }

    function onSaveButtonClick() {
        var styleName = this.inputNametyle.val().trim();

        // The definition of csrfToken is being defined in the brewer.js. more concretely in the Class Security implementation
        // the idea is that this code be centralized, and not duplicate.
        // So this implementation works as an interceptor / filter,
        // which adds the HEADERS to all AJAX post requests before they are submitted

        /*
        var csrfToken = $('input[name=_csrf]').val();
        var header = $('input[name=_csrf_header]').val();

        headers: {
            X-CSRF-TOKEN: dadb40d4-33eb-4c4e-94be-38d193b8be5a
        },
        */


        $.ajax({
            url: this.actionUrl,
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({id: null, name: styleName}),
            error: onSaveStyleError.bind(this),
            success: onStyleSaved.bind(this)
        });
    }

    function onSaveStyleError(obj) {
        var errorMessage = obj.responseText;
        this.containerMessagesError.removeClass('hidden');
        this.containerMessagesError.html('<span>' + errorMessage + '</span>');
        this.form.find('.form-group').addClass('has-error');
    }

    function onStyleSaved(style) {
        var stylesCombo = $('#styles');
        stylesCombo.append('<option value=' + style.id + '>' + style.name + '</option>');
        stylesCombo.val(style.id);
        this.modal.modal('hide');
    }

    return FastStyleRegiste;

}());

$(function () {
    var fastStyleRegister = new Brewer.FastStyleRegiste();
    fastStyleRegister.initialize();
});