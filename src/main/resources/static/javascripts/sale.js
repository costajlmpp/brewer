Brewer.Sale = (
    function () {

        function Sale(salesItemsTable) {
            this.salesItemsTable = salesItemsTable;
            this.saleTotalValueBox = $('.js-sale-total-value-box');
            this.portsValueBox = $('#portsValueBox');
            this.discountValueBox = $('#discountValueBox');

            this.valueItemsTotalValues = 0;
            this.portsValue = 0;
            this.discountValue = 0;
        }

        Sale.prototype.initialize = function () {
            this.salesItemsTable.on('sale-table-item-updated', onTableItemsUpdated.bind(this));
            this.portsValueBox.on('keyup', onPostValueChanged.bind(this));
            this.discountValueBox.on('keyup', discountValueBoxChanged.bind(this));

            this.salesItemsTable.on('sale-table-item-updated', onAnyValueChanged.bind(this));
            this.portsValueBox.on('keyup', onAnyValueChanged.bind(this));
            this.discountValueBox.on('keyup', onAnyValueChanged.bind(this));
        };

        function onPostValueChanged(event) {
            var formattedValue = $(event.target).val();
            this.portsValue = Brewer.parseValue(formattedValue);
            if (this.portsValue == null) this.portsValue = 0;
        }

        function discountValueBoxChanged(event) {
            var formattedValue = $(event.target).val();
            this.discountValue = Brewer.parseValue(formattedValue);
            if (this.discountValue == null) this.discountValue = 0;
        }

        function onTableItemsUpdated(event, totalValue) {
            var v = totalValue == null ? 0 : totalValue;
            this.valueItemsTotalValues = Brewer.parseValue(v);
        }

        function onAnyValueChanged() {
            console.log('--calc :', this.valueItemsTotalValues, this.portsValue, this.discountValue);

            var valorTotal = this.valueItemsTotalValues + this.portsValue - this.discountValue;
            this.saleTotalValueBox.html(Brewer.formatCurrency(valorTotal));

            this.saleTotalValueBox.toggleClass('negative', valorTotal < 0);
        }

        return Sale;
    }());


$(function () {
    var saleBeerAutoComplete = new Brewer.SaleBeerAutoComplete();
    saleBeerAutoComplete.initialize();

    var salesItemsTable = new Brewer.SalesItemsTable(saleBeerAutoComplete);
    salesItemsTable.initialize();

    var sale = new Brewer.Sale(salesItemsTable);
    sale.initialize();
});