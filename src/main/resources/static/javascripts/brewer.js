var Brewer = Brewer || {};

Brewer.MaskMoney = (function () {

    function MaskMoney() {
        this.decimalMoney = $('.js-money');
        this.integerPain = $('.js-integer');
    }

    MaskMoney.prototype.enable = function () {
        this.decimalMoney.maskMoney({thousands: '', decimal: '.', allowNegative: false, allowZero: true});
        this.integerPain.maskMoney({precision: 0});
    };

    return MaskMoney;

}());

Brewer.MaskPhoneNumber = (function () {

    function MaskPhoneNumber() {
        this.inputPhoneNumber = $('.js-phone-number');
    }

    MaskPhoneNumber.prototype.enable = function () {
        var maskBehavior = function (val) {
            // val is the current value of the input, we can use it to conditional masks
            return '(+351) 000-000-000';
        };

        var options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(maskBehavior.apply({}, arguments), options);
            }
        };

        this.inputPhoneNumber.mask(maskBehavior, options);
    };

    return MaskPhoneNumber;

}());

Brewer.MaskNif = (function () {

    function MaskNif() {
        this.nifPain = $('.js-nif');
    }

    MaskNif.prototype.enable = function () {
        this.nifPain.maskMoney({precision: 0, allowNegative: false, allowZero: false, thousands: '', decimal: ''});
    };

    return MaskNif;

}());


Brewer.MaskDate = (function () {

    function MaskDate() {
        this.dateInput = $('.js-date');
    }

    MaskDate.prototype.enable = function () {
        this.dateInput.mask('0000-00-00');
        this.dateInput.datepicker({
            format: 'yyyy-mm-dd',
            clearBtn: true,
            orientation: 'bottom',
            //language: 'pt-PT',
            autoclose: true,
            todayHighlight: true
        });
    };

    return MaskDate;
}());


Brewer.Security = (function () {

    function Security() {
        this.token = $('input[name=_csrf]').val();
        this.header = $('input[name=_csrf_header]').val();
    }

    Security.prototype.enable = function () {
        $(document).ajaxSend(function (event, jqxhr, settings) {
            jqxhr.setRequestHeader(this.header, this.token);
        }.bind(this));
    };

    return Security;

}());

Brewer.formatCurrency = function (value) {
    return numeral(value).format('0,0.00');
    //return numeral(value).format('€0,0.00');
};

Brewer.parseValue = function (formattedValue) {
    return numeral(formattedValue).value()
    //return numeral().unformat(formattedValue);
};

$(function () {
    var maskNumbers = new Brewer.MaskMoney();
    maskNumbers.enable();

    var maskPhoneNumber = new Brewer.MaskPhoneNumber();
    maskPhoneNumber.enable();

    var maskNif = new Brewer.MaskNif();
    maskNif.enable();

    var maskDate = new Brewer.MaskDate();
    maskDate.enable();

    var security = new Brewer.Security();
    security.enable();
});