Brewer = Brewer || {};

Brewer.Multiselection = (function () {

    function Multiselection() {
        this.statusBtn = $('.js-status-btn');
        this.selectableCheckboxs = $('.js-row-selectable');
        this.selectAll = $('.js-select-all-rows');
    }

    Multiselection.prototype.initialize = function () {
        this.statusBtn.on('click', onStatusBtnClick.bind(this));
        this.selectAll.on('click', onSelectAllClick.bind(this));
        this.selectableCheckboxs.on('click', onSelectableClick.bind(this));
    };

    function onStatusBtnClick(event) {
        var btn = $(event.currentTarget);
        var status = btn.data('status');
        var active = 'Y' === status;
        var url = btn.data('url');

        var selectedRows = this.selectableCheckboxs.filter(':checked');
        var identifiers = $.map(selectedRows, function (c) {
            return $(c).data('identifier');
        });

        if (identifiers.length > 0) {
            $.ajax({
                url: url,
                method: 'PUT',
                data: {
                    ids: identifiers,
                    active: active
                },
                success: function () {
                    window.location.reload();
                }
            });
        }
    }

    function onSelectAllClick(event) {
        var isChecked = this.selectAll.prop('checked');
        this.selectableCheckboxs.prop('checked', isChecked);

        changeActionButtonsStatus.call(this, isChecked);
    }

    function onSelectableClick(event) {
        var selectedCheckBoxs = this.selectableCheckboxs.filter(':checked');
        var isChecked = selectedCheckBoxs.length >= this.selectableCheckboxs.length;
        this.selectAll.prop('checked', isChecked);

        var enableBtn = selectedCheckBoxs.length > 0;

        changeActionButtonsStatus.call(this, enableBtn);
    }

    function changeActionButtonsStatus(enable) {
        if (enable) {
            this.statusBtn.removeClass('disabled');
            this.statusBtn.removeAttr('disabled');
        } else {
            this.statusBtn.addClass('disabled');
            this.statusBtn.attr('disabled', 'disabled');
        }
    }

    return Multiselection;

}());

$(function () {

    var multiselection = new Brewer.Multiselection();
    multiselection.initialize();

});

