Brewer.SalesItemsTable = (
    function () {

        function SalesItemsTable(saleBeerAutoComplete) {
            this.saleBeerAutoComplete = saleBeerAutoComplete;
            this.tableBeersContainer = $(".js-table-beers-container");
            this.eventEmitter = $({});
            this.on = this.eventEmitter.on.bind(this.eventEmitter);
        }

        SalesItemsTable.prototype.initialize = function () {
            this.saleBeerAutoComplete.on('sales-item-table-selected-item', onSelectedItem.bind(this));
        };

        function onSelectedItem(event, itemData) {
            var response = $.ajax({
                url: 'item',
                method: 'POST',
                data: {
                    beerId: itemData.id

                }
            });

            response.done(onItemAdded.bind(this));
        }

        function onItemAdded(html) {
            //console.log('----' + html);
            // if we need to use The keyword inside other function we must di a bind(this)
            this.tableBeersContainer.html(html);

            // add listener on the qty change
            var qtyItemInput = $(".js-table-beer-qty");
            qtyItemInput.on('change', onItemQtyChanged.bind(this));
            qtyItemInput.maskMoney({ precision: 0, thousands: '' });

            var saleTableItem = $('.js-table-item');
            saleTableItem.on('dblclick', onDoubleClick);
            $('.js-table-item__remove-painel-btn').on('click', removeItemClick.bind(this))

            // total-value
            var totalValue = saleTableItem.data('total-value');
            this.eventEmitter.trigger('sale-table-item-updated', totalValue);
        }

        function removeItemClick(event) {
            var beerId = $(event.target).attr('beerId');

            console.log('deleting --->' + beerId);

            var response = $.ajax({
                url: 'item/' + beerId,
                method: 'DELETE'
            });

            response.done(onItemAdded.bind(this));
        }
        
        function onDoubleClick(event) {
            // if we use event.target we use the any element inside of the component,
            // but in this case we need to use the root element that are resisting the event
            // other note: $(this) is e $(event.currentTarget)
            //$(event.currentTarget);
            $(this).toggleClass('requesting-exclusion');
        }

        function onItemQtyChanged(event) {
            var input = $(event.target);
            var qty = input.val();
            //var beerId = input.data('beer-id');
            var beerId = input.attr('beerId');

            if (qty <= 0) {
                input.val(1);
                qty = 1;
            }

            var response = $.ajax({
                url: 'item/' + beerId,
                method: 'PUT',
                data: {
                    qty: qty
                }
            });

            response.done(onItemAdded.bind(this));
        }

        return SalesItemsTable;

    }());

$(function () {
    var saleBeerAutoComplete = new Brewer.SaleBeerAutoComplete();
    saleBeerAutoComplete.initialize();

    var salesItemsTable = new Brewer.SalesItemsTable(saleBeerAutoComplete);
    salesItemsTable.initialize();
});
