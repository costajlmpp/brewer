package io.costax.brewer.application;

import io.costax.brewer.domain.model.client.Client;

public interface ClientService {

    Client save(Client client);
}
