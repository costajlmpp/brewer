package io.costax.brewer.application;

import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.model.sale.Sale;

public interface SaleService {

    void create(User user, Sale sale);
}
