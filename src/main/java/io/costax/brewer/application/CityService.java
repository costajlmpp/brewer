package io.costax.brewer.application;

import io.costax.brewer.domain.model.client.City;

public interface CityService {

    City save(City city);
}
