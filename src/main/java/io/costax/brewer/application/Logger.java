package io.costax.brewer.application;

import java.util.function.Supplier;

public interface Logger {

    void debug(Supplier<String> message);

    void debug(String template, Object... params);

    void info(Supplier<String> message);

    void info(String template, Object... params);

}
