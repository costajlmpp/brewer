package io.costax.brewer.application.internal;

import io.costax.brewer.application.UserService;
import io.costax.brewer.domain.model.profiles.EmailAlreadyRegistedException;
import io.costax.brewer.domain.model.profiles.PasswordUndefinedException;
import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.repositories.Strings;
import io.costax.brewer.domain.repositories.profile.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Service
public class DefaultUserService implements UserService {

    private final Users users;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DefaultUserService(Users users, PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public User save(User user) {

        final boolean present = users.findUserByEmailIgnoreCase(user.getEmail()).isPresent();
        if (present) {
            throw new EmailAlreadyRegistedException(user.getEmail());
        }

        if (user.isNew() && Strings.isBlank(user.getPassword())) {
            throw new PasswordUndefinedException();
        }

        if (user.isNew()) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setPasswordConfirmation(user.getPassword());
        }

        final User saved = users.save(user);
        users.flush();

        return saved;
    }

    @Override
    @Transactional
    public void changeStatus(final Integer[] usersIds, final Boolean active) {
        if (usersIds != null && usersIds.length > 0) {
            users.updateStatus(Arrays.asList(usersIds), active);
        }
    }
}
