package io.costax.brewer.application.internal;

import io.costax.brewer.application.SaleService;
import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.model.sale.Sale;
import io.costax.brewer.domain.repositories.beer.Beers;
import io.costax.brewer.domain.repositories.client.Clients;
import io.costax.brewer.domain.repositories.profile.Users;
import io.costax.brewer.domain.repositories.sale.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DefaultSaleService implements SaleService {

    @Autowired
    Sales sales;

    @Autowired
    Clients clients;

    @Autowired
    Beers beers;

    @Autowired
    Users users;

    @Transactional
    public void create(User user, Sale sale) {
        Client client = clients.getOne(sale.getClient().getId());

        Sale newSale = Sale.empty(client, user);
        newSale.setDiscountValue(sale.getDiscountValue());
        newSale.setPortsValue(sale.getPortsValue());

        sale.getItems().forEach(si -> newSale.add(beers.getOne(si.getBeer().getId()), si.getQty()));

        sales.save(newSale);
    }

}
