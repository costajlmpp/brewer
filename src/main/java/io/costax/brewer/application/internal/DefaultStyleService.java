package io.costax.brewer.application.internal;

import io.costax.brewer.application.StyleService;
import io.costax.brewer.domain.model.beer.Style;
import io.costax.brewer.domain.model.beer.StyleNameAlreadyRegisteredException;
import io.costax.brewer.domain.repositories.beer.Styles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class DefaultStyleService implements StyleService {

    private final Styles styles;

    @Autowired
    public DefaultStyleService(Styles styles) {
        this.styles = styles;
    }

    @Override
    @Transactional
    public Style save(Style style) {
        final Optional<Style> existingWithSameName = styles.findByNameIgnoreCase(style.getName());

        if (existingWithSameName.isPresent()) {
            throw new StyleNameAlreadyRegisteredException(style.getName());
        }

        return styles.saveAndFlush(style);
    }
}
