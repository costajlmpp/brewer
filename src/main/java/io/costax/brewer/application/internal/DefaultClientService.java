package io.costax.brewer.application.internal;

import io.costax.brewer.application.ClientService;
import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.client.NifAlreadyRegisteredException;
import io.costax.brewer.domain.repositories.client.Clients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class DefaultClientService implements ClientService {

    private final Clients clients;

    @Autowired
    public DefaultClientService(Clients clients) {
        this.clients = clients;
    }

    @Override
    @Transactional
    public Client save(Client client) {
        Optional<Client> other = clients.findByNif(client.getNif());

        if (other.isPresent()) {
            throw new NifAlreadyRegisteredException(client.getNif());
        }

        return clients.saveAndFlush(client);
    }
}
