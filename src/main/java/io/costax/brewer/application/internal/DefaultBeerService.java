package io.costax.brewer.application.internal;

import io.costax.brewer.application.BeerService;
import io.costax.brewer.application.events.beer.BeerCreated;
import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.repositories.beer.Beers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DefaultBeerService implements BeerService {

    private final Beers beers;
    private final ApplicationEventPublisher publisher;

    @Autowired
    public DefaultBeerService(Beers beers, ApplicationEventPublisher publisher) {
        this.beers = beers;
        this.publisher = publisher;
    }

    @Override
    @Transactional
    public void save(Beer beer) {
        if (beer.getImage() != null && !beer.hasImageDefined()) {
            beer.removeImage();
        }

        beers.save(beer);

        publisher.publishEvent(new BeerCreated(beer));
    }
}
