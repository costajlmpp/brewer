package io.costax.brewer.application.internal;


import io.costax.brewer.application.Logger;

import java.util.function.Supplier;

public class LoggerBridge implements Logger {

    public final org.slf4j.Logger log;

    private LoggerBridge(org.slf4j.Logger log) {
        this.log = log;
    }

    public static LoggerBridge of(org.slf4j.Logger log) {
        return new LoggerBridge(log);
    }

    @Override
    public void debug(Supplier<String> message) {
        if (log.isDebugEnabled()) {
            debug(message.get());
        }
    }

    @Override
    public void debug(String template, Object... params) {
        if (log.isDebugEnabled()) {
            log.debug(template, params);
        }
    }

    @Override
    public void info(Supplier<String> message) {
        if (log.isInfoEnabled()) {
            info(message.get());
        }
    }

    @Override
    public void info(String template, Object... params) {
        if (log.isInfoEnabled()) {
            log.info(template, params);
        }
    }
}
