package io.costax.brewer.application.internal;

import io.costax.brewer.application.storage.Storage;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

import static java.nio.file.FileSystems.getDefault;

public class LocalStorage implements Storage {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalStorage.class);
    private static final String BREWER_IMAGES = "brewerimages";

    private final Path local;
    private Path tempLocal;

    public LocalStorage() {
        this.local = getDefault().getPath(System.getenv("HOME"), BREWER_IMAGES);
        createFolders();
    }

    private void createFolders() {
        try {
            Files.createDirectories(this.local);
            this.tempLocal = getDefault().getPath(this.local.toString(), "temp");
            Files.createDirectories(this.tempLocal);


            LOGGER.debug("Local Storage Folders Created: '{}'", local.toAbsolutePath());


        } catch (IOException e) {
            throw new UncheckedIOException("Could not create the images folders", e);
        }
    }

    @Override
    public String storeTemporarily(MultipartFile file) {
        try {
            final String newName = storeFileName(file.getOriginalFilename());
            file.transferTo(new File(tempLocal.toAbsolutePath().toString() + getDefault().getSeparator() + newName));
            return newName;
        } catch (IOException e) {
            throw new UncheckedIOException("Error store Temporarily file", e);
        }
    }

    @Override
    public void store(String fileName) {
        try {
            final File file = new File(tempLocal.toAbsolutePath().toString() + getDefault().getSeparator() + fileName);
            if (file.exists()) {
                final File destination = new File(local.toAbsolutePath().toString() + getDefault().getSeparator() + fileName);
                Files.move(file.toPath(), destination.toPath(), StandardCopyOption.ATOMIC_MOVE);


                Thumbnails.of(this.local.resolve(fileName).toString())
                        .size(40, 68).toFiles(Rename.PREFIX_DOT_THUMBNAIL);
            }
        } catch (IOException e) {
            throw new UncheckedIOException("Error saving file '" + fileName + "'", e);
        }
    }

    @Override
    public Optional<byte[]> getTemporaryDoc(String fileName) {
        try {
            final byte[] bytes = Files.readAllBytes(this.tempLocal.resolve(fileName));
            return Optional.of(bytes);
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<byte[]> getDoc(String fileName) {
        try {
            final byte[] bytes = Files.readAllBytes(this.local.resolve(fileName));
            return Optional.of(bytes);
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private String storeFileName(String fileName) {
        return UUID.randomUUID() + "-" + fileName;
    }
}
