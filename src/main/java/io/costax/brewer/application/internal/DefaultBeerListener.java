package io.costax.brewer.application.internal;

import io.costax.brewer.application.events.beer.BeerCreated;
import io.costax.brewer.application.storage.Storage;
import io.costax.brewer.domain.model.beer.Beer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DefaultBeerListener {

    private final Storage storage;

    @Autowired
    public DefaultBeerListener(Storage storage) {
        this.storage = storage;
    }

    @EventListener(condition = "#event.beer.hasImageDefined()")
    public void onBeerCreated(BeerCreated event) {

        Optional.ofNullable(event.getBeer())
                .flatMap(Beer::getImageName)
                .ifPresent(storage::store);
    }
}
