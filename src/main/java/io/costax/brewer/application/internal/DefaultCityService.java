package io.costax.brewer.application.internal;

import io.costax.brewer.application.CityService;
import io.costax.brewer.domain.model.client.City;
import io.costax.brewer.domain.repositories.client.Cities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DefaultCityService implements CityService {

    private final Cities cities;

    @Autowired
    public DefaultCityService(Cities cities) {
        this.cities = cities;
    }

    @Override
    @Transactional
    public City save(City city) {
        return cities.save(city);
    }
}
