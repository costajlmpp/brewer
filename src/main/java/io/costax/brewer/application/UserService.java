package io.costax.brewer.application;

import io.costax.brewer.domain.model.profiles.User;

public interface UserService {

    User save(User user);

    void changeStatus(Integer[] usersIds, Boolean active);
}
