package io.costax.brewer.application;

import io.costax.brewer.domain.model.beer.Style;

public interface StyleService {

    Style save(Style style);
}
