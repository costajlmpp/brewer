package io.costax.brewer.application;

import io.costax.brewer.domain.model.beer.Beer;

public interface BeerService {

    void save(Beer beer);
}
