package io.costax.brewer.application;

import io.costax.brewer.domain.model.sale.Sale;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SaleValidator implements Validator {

    @Override
    public boolean supports(final Class<?> aClass) {
        return Sale.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "client", "", "the client should be defined");

    }
}
