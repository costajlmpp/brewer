package io.costax.brewer.application.events.beer;

import io.costax.brewer.domain.model.beer.Beer;

public class BeerCreated {

    private final Beer beer;

    public BeerCreated(Beer beer) {
        this.beer = beer;
    }

    public Beer getBeer() {
        return beer;
    }
}
