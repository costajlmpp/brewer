package io.costax.brewer.application.storage;

import io.costax.brewer.domain.model.beer.Image;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

public class StoreImageRunnable implements Runnable {

    private final MultipartFile[] files;
    private final DeferredResult<Image> result;
    private final Storage storage;

    public StoreImageRunnable(MultipartFile[] files, DeferredResult<Image> result, Storage storage) {
        this.files = files;
        this.result = result;
        this.storage = storage;
    }

    @Override
    public void run() {
        if (files.length < 1) {
            this.result.setErrorResult("NO Images send");
        } else {
            final MultipartFile file = files[0];
            final String saveFileName = this.storage.storeTemporarily(file);

            result.setResult(Image.of(saveFileName, file.getContentType()));
        }
    }
}
