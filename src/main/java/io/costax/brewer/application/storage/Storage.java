package io.costax.brewer.application.storage;

import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface Storage {

    String storeTemporarily(MultipartFile file);

    void store(String fileName);

    Optional<byte[]> getTemporaryDoc(String name);

    Optional<byte[]> getDoc(String name);
}
