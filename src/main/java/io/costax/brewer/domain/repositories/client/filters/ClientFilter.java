package io.costax.brewer.domain.repositories.client.filters;

public class ClientFilter {

    private String name;

    private String nif;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }
}
