package io.costax.brewer.domain.repositories.client;

import io.costax.brewer.domain.model.client.City;
import io.costax.brewer.domain.repositories.client.helpers.CitiesQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface Cities extends JpaRepository<City, Long>, CitiesQueries {

    @Query("select c from City c join fetch c.country where c.country.id = :countryId")
    List<City> findByCountryId(@Param("countryId") Long countryId);
}
