package io.costax.brewer.domain.repositories;

public final class Strings {

    private Strings() {
        throw new AssertionError();
    }

    public static boolean isNotBlank(String value) {
        return !isBlank(value);
    }

    public static boolean isBlank(String value) {
        return value == null || value.trim().isEmpty();
    }

}
