package io.costax.brewer.domain.repositories.client.helpers;


import io.costax.brewer.domain.model.client.*;
import io.costax.brewer.domain.repositories.CriteriaMethods;
import io.costax.brewer.domain.repositories.Strings;
import io.costax.brewer.domain.repositories.client.filters.ClientFilter;
import org.hibernate.annotations.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static io.costax.brewer.domain.repositories.CriteriaMethods.MatchMode;
import static io.costax.brewer.domain.repositories.Strings.isNotBlank;

public class ClientsImpl implements ClientQueries {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CriteriaMethods helper;

    @Override
    public List<ClientSummary> findByName(final String name) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<ClientSummary> cq = cb.createQuery(ClientSummary.class);
        final Root<Client> c = cq.from(Client.class);

        List<Predicate> predicates = new ArrayList<>();

        if (Strings.isNotBlank(name)) {
            predicates.add(helper.ilike(cb, c.get(Client_.name), name, MatchMode.START));
        }

        //@formatter:off
        return em.createQuery(
                        cq.select(cb.construct(
                                ClientSummary.class,
                                c.get(Client_.id),
                                c.get(Client_.name),
                                c.get(Client_.phone)))
                                .distinct(true)
                        .where(predicates.toArray(new Predicate[0]))
                        .orderBy(cb.asc(c.get(Client_.id)))
                ).setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .setHint(QueryHints.READ_ONLY, true)
                .getResultList();
        //@formatter:on
    }

    @Override
    public Page<Client> search(ClientFilter filter, Pageable pageable) {
        return new PageImpl<>(getSearchResult(filter, pageable), pageable, total(filter));
    }

    private Long total(ClientFilter filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Client> clients = cq.from(Client.class);

        cq.select(cb.count(clients.get(Client_.id))).where(filterToPredicate(filter, cb, clients));
        final TypedQuery<Long> query = em.createQuery(cq);

        return query.getSingleResult();
    }

    private List<Client> getSearchResult(ClientFilter filter, Pageable pageable) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Client> cq = cb.createQuery(Client.class);
        Root<Client> clients = cq.from(Client.class);
        clients.fetch(Client_.address)
                .fetch(Address_.city, JoinType.LEFT)
                .fetch(City_.country, JoinType.LEFT);

        Predicate[] predicates = filterToPredicate(filter, cb, clients);

        List<Order> orders = helper.ordersOf(pageable, cb, clients,
                new HashSet<>(Arrays.asList(Client_.NAME, Client_.NIF, Client_.PHONE)));

        return em.createQuery(
                cq.select(clients).distinct(true)
                        .where(predicates).orderBy(orders))
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    private Predicate[] filterToPredicate(ClientFilter filter, CriteriaBuilder cb, Root<Client> clients) {
        List<Predicate> predicates = new ArrayList<>(2);

        if (isNotBlank(filter.getName())) {
            Predicate name = helper.ilike(cb, clients.get(Client_.name), filter.getName(), MatchMode.START);
            predicates.add(name);
        }

        if (isNotBlank(filter.getNif())) {
            Predicate nif = helper.ilike(cb, clients.get(Client_.nif).as(String.class), filter.getNif(), MatchMode.START);
            predicates.add(nif);
        }

        return predicates.toArray(new Predicate[0]);
    }
}
