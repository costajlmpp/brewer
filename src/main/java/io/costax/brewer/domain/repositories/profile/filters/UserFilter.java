package io.costax.brewer.domain.repositories.profile.filters;

import io.costax.brewer.domain.model.profiles.UserGroup;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UserFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String email;
    private Set<UserGroup> groups = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Set<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(final Set<UserGroup> groups) {
        this.groups = groups;
    }
}
