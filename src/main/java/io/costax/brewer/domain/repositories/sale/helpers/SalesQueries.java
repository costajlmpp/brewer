package io.costax.brewer.domain.repositories.sale.helpers;

import io.costax.brewer.domain.model.sale.SaleSummary;
import io.costax.brewer.domain.repositories.sale.filters.SaleFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SalesQueries {
    Page<SaleSummary> search(SaleFilter filter, Pageable pageable);
}
