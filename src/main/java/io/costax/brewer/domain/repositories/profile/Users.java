package io.costax.brewer.domain.repositories.profile;

import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.repositories.profile.helpers.UsersQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;

@Repository
public interface Users extends JpaRepository<User, Long>, UsersQueries {

    Optional<User> findUserByEmailIgnoreCase(String email);

    @Query("select distinct u " +
            "from User u " +
            "left join fetch u.groups g " +
            "left join fetch g.permissions " +
            "where lower(trim( u.email)) = lower( trim( :email) ) and u.enable = true")
    @QueryHints({@QueryHint(name = org.hibernate.jpa.QueryHints.HINT_PASS_DISTINCT_THROUGH, value = "false")})
    Optional<User> findUserByEmailIgnoreCaseAndEnable(@Param("email") String email);

    @Modifying
    @Query(value = "update user u set u.enable = :active where u.id in ( :ids ) ", nativeQuery = true)
    void updateStatus(@Param("ids") List<Integer> ids,
                      @Param("active") Boolean active);
}
