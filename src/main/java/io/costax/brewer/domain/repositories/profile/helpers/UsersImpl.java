package io.costax.brewer.domain.repositories.profile.helpers;

import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.model.profiles.UserGroup;
import io.costax.brewer.domain.model.profiles.UserGroup_;
import io.costax.brewer.domain.model.profiles.User_;
import io.costax.brewer.domain.repositories.CriteriaMethods;
import io.costax.brewer.domain.repositories.profile.filters.UserFilter;
import org.hibernate.annotations.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class UsersImpl implements UsersQueries {

    @Autowired
    CriteriaMethods criteriaMethods;

    @PersistenceContext
    EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public Page<User> search(final UserFilter filter, Pageable pageable) {
        return new PageImpl<>(getSearchResult(filter, pageable), pageable, total(filter));
    }

    private Long total(final UserFilter filter) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        final Root<User> users = cq.from(User.class);

        List<Predicate> predicates = getPredicates(filter, cb, users, cq);

        //@formatter:off
        return em.createQuery(
                cq.select(cb.count(users.get(User_.id)))
                .where(predicates.toArray(new Predicate[0]))
            ).getSingleResult();
        //@formatter:on
    }

    private List<User> getSearchResult(final UserFilter filter, final Pageable pageable) {

        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<User> cq = cb.createQuery(User.class);
        final Root<User> users = cq.from(User.class);
        users.fetch(User_.groups, JoinType.LEFT);

        List<Predicate> predicates = getPredicates(filter, cb, users, cq);

        //@formatter:off
        return em.createQuery(
                    cq.distinct(true).where(predicates.toArray(new Predicate[0]))
                ).setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .setMaxResults(pageable.getPageSize())
                .setFirstResult(pageable.getPageSize() * pageable.getPageNumber())
                .getResultList();
        //@formatter:on
    }

    private List<Predicate> getPredicates(final UserFilter filter,
                                          final CriteriaBuilder cb,
                                          final Root<User> users,
                                          final CriteriaQuery masterCriteriaQuery) {
        List<Predicate> predicates = new ArrayList<>();
        if (filter != null) {
            if (filter.getName() != null && !filter.getName().trim().isEmpty()) {
                final Predicate names = criteriaMethods.ilike(cb, users.get(User_.name), filter.getName(), CriteriaMethods.MatchMode.ANYWHERE);
                predicates.add(names);
            }

            if (filter.getName() != null && !filter.getName().trim().isEmpty()) {
                final Predicate emails = criteriaMethods.ilike(cb, users.get(User_.email), filter.getEmail(), CriteriaMethods.MatchMode.START);
                predicates.add(emails);
            }

            if (filter.getGroups() != null) {
                for (UserGroup userGroup : filter.getGroups()) {
                    final Subquery<Long> subQuery = masterCriteriaQuery.subquery(Long.class);
                    final Root<User> from = subQuery.from(User.class);
                    final SetJoin<User, UserGroup> groupSetJoin = from.join(User_.groups, JoinType.INNER);

                    //@formatter:off
                    subQuery.select(from.get(User_.id)).distinct(true)
                            .where(cb.equal(groupSetJoin.get(UserGroup_.id), userGroup.getId()));
                    //@formatter:on

                    final CriteriaBuilder.In<Long> value = cb.in(subQuery).value(users.get(User_.id));
                    //final Predicate in = users.in(subquery);

                    predicates.add(value);
                }
            }
        }

        return predicates;
    }
}
