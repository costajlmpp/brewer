package io.costax.brewer.domain.repositories.profile;

import io.costax.brewer.domain.model.profiles.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserGroups extends JpaRepository<UserGroup, Long> {
}
