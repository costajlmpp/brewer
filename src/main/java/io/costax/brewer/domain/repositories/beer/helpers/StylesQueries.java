package io.costax.brewer.domain.repositories.beer.helpers;

import io.costax.brewer.domain.model.beer.Style;
import io.costax.brewer.domain.repositories.beer.filters.StyleFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StylesQueries {

    Page<Style> search(StyleFilter filter, Pageable pageable);
}
