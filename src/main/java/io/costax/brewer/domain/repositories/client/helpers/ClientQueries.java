package io.costax.brewer.domain.repositories.client.helpers;

import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.client.ClientSummary;
import io.costax.brewer.domain.repositories.client.filters.ClientFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClientQueries {

    Page<Client> search(ClientFilter filter, Pageable pageable);

    List<ClientSummary> findByName(String name);

}
