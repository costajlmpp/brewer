package io.costax.brewer.domain.repositories.beer.helpers;

import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.model.beer.BeerSummary;
import io.costax.brewer.domain.repositories.beer.filters.BeerFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * <p>
 * The Name of this interface is very important, because respect the name convections of the spring data jpa.
 * the first part of name (Beers) it is the name of the Repository and the secound part (Queries) have this name
 * intentionality
 * </p>
 * <p>
 * The Implementation of this interface must be called of: BeersImpl
 * </p>
 * unLess we change the configuration of JPA repositories: @EnableJpaRepositories(repositoryImplementationPostfix = "SomeOtherSufix" .... )
 */
public interface BeersQueries {

    Page<Beer> search(BeerFilter filter, Pageable pageable);

    List<BeerSummary> searchSummaryByCodeOrName(String codeOrName);
}
