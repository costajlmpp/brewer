package io.costax.brewer.domain.repositories.sale.helpers;

import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.client.Client_;
import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.model.profiles.User_;
import io.costax.brewer.domain.model.sale.Sale;
import io.costax.brewer.domain.model.sale.SaleSummary;
import io.costax.brewer.domain.model.sale.Sale_;
import io.costax.brewer.domain.repositories.CriteriaMethods;
import io.costax.brewer.domain.repositories.sale.filters.SaleFilter;
import org.hibernate.annotations.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class SalesImpl implements SalesQueries {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CriteriaMethods criteriaMethods;

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<SaleSummary> search(final SaleFilter filter, final Pageable pageable) {
        return new PageImpl<>(getSearchResult(filter, pageable), pageable, total(filter));
    }

    private List<SaleSummary> getSearchResult(final SaleFilter filter, final Pageable pageable) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SaleSummary> cq = cb.createQuery(SaleSummary.class);
        Root<Sale> sales = cq.from(Sale.class);
        Join<Sale, Client> salesClient = sales.join(Sale_.client, JoinType.INNER);
        Join<Sale, User> seller = sales.join(Sale_.user, JoinType.LEFT);

        List<Predicate> predicates = toPredicates(filter, sales, cb);
        List<Order> orders = getOrders(pageable, cb, sales, salesClient);

        //@formatter:off
        return em.createQuery(
                    cq.select(
                            cb.construct(
                                SaleSummary.class,

                                sales.get(Sale_.id),
                                salesClient.get(Client_.name),
                                sales.get(Sale_.createAt),
                                sales.get(Sale_.total),
                                sales.get(Sale_.state),
                                seller.get(User_.name))
                            )
                    .where(predicates.toArray(new Predicate[0]))
                    .orderBy(orders)
                )
                .setHint(QueryHints.READ_ONLY, true)
                .setFirstResult(pageable.getOffset())
                .setMaxResults(pageable.getPageSize())
                .getResultList();
        //@formatter:on
    }

    private List<Order> getOrders(final Pageable pageable, final CriteriaBuilder cb, final Root<Sale> sales, final Join<Sale, Client> salesClient) {
        List<Order> orders = new ArrayList<>();
        Sort sort = pageable.getSort();
        if (sort != null) {

            Sort.Order clientName = sort.getOrderFor("clientName");
            if (clientName != null) {
                orders.add(criteriaMethods.orderBy(cb, salesClient.get(Client_.name), clientName.isAscending()));
            }

            Sort.Order createdAt = sort.getOrderFor("createdAt");
            if (createdAt != null) {
                orders.add( criteriaMethods.orderBy(cb, sales.get(Sale_.createAt), createdAt.isAscending()));
            }

            Sort.Order statusAt = sort.getOrderFor("status");
            if (statusAt != null) {
                orders.add( criteriaMethods.orderBy(cb, sales.get(Sale_.state), statusAt.isAscending()));
            }

            Sort.Order total = sort.getOrderFor("total");
            if (total != null) {
                orders.add( criteriaMethods.orderBy(cb, sales.get(Sale_.total), total.isAscending()));
            }

        } else {
            Order ascById = cb.asc(sales.get(Sale_.id));
            orders.add(ascById);
        }
        return orders;
    }

    private Long total(final SaleFilter filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Sale> sales = cq.from(Sale.class);
        sales.join(Sale_.client, JoinType.INNER);

        List<Predicate> predicates = toPredicates(filter, sales, cb);

        return em.createQuery(cq.select(cb.count(sales.get(Sale_.id))).where(predicates.toArray(new Predicate[0]))).getSingleResult();
    }

    private List<Predicate> toPredicates(final SaleFilter filter, final Root<Sale> sales, final CriteriaBuilder cb) {
        ArrayList<Predicate> predicates = new ArrayList<>();

        if (filter.getCode() != null) {
            Expression<String> codeAsString = sales.get(Sale_.id).as(String.class);

            Predicate ilike = this.criteriaMethods.ilike(cb, codeAsString, filter.getCode(), CriteriaMethods.MatchMode.ANYWHERE);
            predicates.add(ilike);
        }

        return predicates;
    }

}
