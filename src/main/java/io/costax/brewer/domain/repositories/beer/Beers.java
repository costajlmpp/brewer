package io.costax.brewer.domain.repositories.beer;

import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.repositories.beer.helpers.BeersQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Beers extends JpaRepository<Beer, Long>, BeersQueries {

    Optional<Beer> findBeerByCodeIgnoreCase(String code);
}
