package io.costax.brewer.domain.repositories.profile.helpers;

import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.repositories.profile.filters.UserFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UsersQueries {

    Page<User> search(UserFilter filter, Pageable pageable);
}
