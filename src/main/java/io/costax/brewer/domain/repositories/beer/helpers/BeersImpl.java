package io.costax.brewer.domain.repositories.beer.helpers;

import io.costax.brewer.domain.model.beer.*;
import io.costax.brewer.domain.repositories.CriteriaMethods;
import io.costax.brewer.domain.repositories.beer.filters.BeerFilter;
import org.hibernate.annotations.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.*;

public class BeersImpl implements BeersQueries {

    private static final Set<String> ORDER_PROPERTIES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("name", "code", "value")));
    private final CriteriaMethods criteriaMethods;
    @PersistenceContext
    private EntityManager manager;

    @Autowired
    BeersImpl(CriteriaMethods criteriaMethods) {
        this.criteriaMethods = criteriaMethods;
    }

    /**
     * Same time this methods must be marked with @org.springframework.transaction.annotation.Transactional(readOnly = true), if we use Hibernate session
     */
    @Override
    public Page<Beer> search(BeerFilter filter, Pageable pageable) {
        return new PageImpl<>(getBeers(filter, pageable), pageable, total(filter));
    }

    @Override
    public List<BeerSummary> searchSummaryByCodeOrName(final String codeOrName) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<BeerSummary> cq = cb.createQuery(BeerSummary.class);
        Root<Beer> b = cq.from(Beer.class);

        String codeOrNameOrEmpty = Optional.ofNullable(codeOrName).orElse("");

        //@formatter:off
        return manager.createQuery(
                cq.select(
                    cb.construct(BeerSummary.class,
                        b.get(Beer_.id),
                        b.get(Beer_.code),
                        b.get(Beer_.name),
                        b.get(Beer_.origin),
                        b.get(Beer_.value),
                        b.get(Beer_.image).get(Image_.name)))
                .where(
                    cb.or(
                        criteriaMethods.ilike(cb, b.get(Beer_.code), codeOrNameOrEmpty, CriteriaMethods.MatchMode.START),
                        criteriaMethods.ilike(cb, b.get(Beer_.name), codeOrNameOrEmpty, CriteriaMethods.MatchMode.START)
                    )
                )
                .orderBy(cb.asc(b.get(Beer_.id)))
            ).setHint(QueryHints.READ_ONLY, true)
            .getResultList();
        //@formatter:on
    }

    private List<Beer> getBeers(BeerFilter filter, Pageable pageable) {
        final CriteriaBuilder cb = manager.getCriteriaBuilder();
        final CriteriaQuery<Beer> c = cb.createQuery(Beer.class);

        final Root<Beer> beers = c.from(Beer.class);
        @SuppressWarnings("unchecked")
        Join<Beer, Style> style = (Join) beers.fetch("style", JoinType.LEFT);

        List<Predicate> predicates = toPredicates(filter, cb, beers, style);

        List<Order> orders = criteriaMethods.ordersOf(pageable, cb, beers, ORDER_PROPERTIES);

        return manager.createQuery(
                c.select(beers).distinct(true)
                        .where(predicates.toArray(new Predicate[0]))
                        .orderBy(orders))
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    private Long total(BeerFilter filter) {
        final CriteriaBuilder cb = manager.getCriteriaBuilder();
        final CriteriaQuery<Long> c = cb.createQuery(Long.class);

        final Root<Beer> beers = c.from(Beer.class);
        @SuppressWarnings("unchecked")
        Join<Beer, Style> style = beers.join("style", JoinType.LEFT);


        List<Predicate> predicates = toPredicates(filter, cb, beers, style);

        return manager.createQuery(
                c.select(cb.count(beers.get("id"))).where(predicates.toArray(new Predicate[0]))).getSingleResult();
    }

    private List<Predicate> toPredicates(BeerFilter filter, CriteriaBuilder cb, Root<Beer> beers, Join<Beer, Style> style) {
        List<Predicate> predicates = new ArrayList<>();
        if (filter != null) {

            if (criteriaMethods.isNotBlank(filter.getName())) {
                final String pattern = filter.getName() + "%";
                final Predicate nameFilter = cb.like(cb.upper(beers.get("name")), pattern.toUpperCase());
                predicates.add(nameFilter);
            }

            if (criteriaMethods.isNotBlank(filter.getCode())) {
                final Predicate codeFilter = cb.equal(cb.upper(beers.get("code")), filter.getCode().trim().toUpperCase());
                predicates.add(codeFilter);
            }

            if (filter.getStyle() != null) {
                final Predicate styleFilter = cb.equal(style, filter.getStyle());
                predicates.add(styleFilter);
            }

            if (filter.getFlavor() != null) {
                final Predicate flavorFilter = cb.equal(beers.get("flavor"), filter.getFlavor());
                predicates.add(flavorFilter);
            }

            if (filter.getOrigin() != null) {
                predicates.add(cb.equal(beers.get("origin"), filter.getOrigin()));
            }

            if (filter.getValueSince() != null) {
                predicates.add(cb.greaterThanOrEqualTo(beers.get("value"), filter.getValueSince()));
            }

            if (filter.getValueUpTo() != null) {
                predicates.add(cb.lessThanOrEqualTo(beers.get("value"), filter.getValueUpTo()));
            }
        }
        return predicates;
    }
}
