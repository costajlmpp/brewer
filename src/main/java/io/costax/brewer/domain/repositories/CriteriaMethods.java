package io.costax.brewer.domain.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.Arrays.asList;

@Component
public class CriteriaMethods {

    public List<Order> ordersOf(Pageable pageable, CriteriaBuilder cb, Root root, String... orderProperties) {
        if (orderProperties.length == 0) {
            return Collections.emptyList();
        }

        final Set<String> names = new HashSet<>(asList(orderProperties));

        return ordersOf(pageable, cb, root, names);
    }

    public List<Order> ordersOf(Pageable pageable, CriteriaBuilder cb, Root root, Set<String> orderProperties) {
        return Optional.ofNullable(pageable.getSort())
                .map(p -> StreamSupport.stream(Spliterators.spliteratorUnknownSize(p.iterator(), Spliterator.ORDERED), false)
                        .filter(o -> orderProperties.contains(o.getProperty()))
                        .map(o -> toOrder(cb, root, o.getDirection(), o.getProperty()))
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    public boolean isNotBlank(String value) {
        return value != null && !value.trim().isEmpty();
    }

    private Order toOrder(CriteriaBuilder cb, Root root, Sort.Direction direction, String property) {
        switch (direction) {
            case DESC:
                return cb.desc(root.get(property));
            default:
                return cb.asc(root.get(property));
        }
    }

    public Order toOrder(CriteriaBuilder cb, Expression yPath, Sort.Direction direction) {
        switch (direction) {
            case DESC:
                return cb.desc(yPath);
            default:
                return cb.asc(yPath);
        }

    }


    /**
     * use this method when you need to get a SingleResult, but you don't have sure
     * that exist and you don't want handler the {@code NoResultException}.
     * Uncheck the Throws of NoResultException.
     *
     * @param <T>        the generic type
     * @param typedQuery the typed query
     * @return the t - instance of T if exists, {@code null} otherwise.
     */
    public <T> T getSingleResultUncheck(final TypedQuery<T> typedQuery) {
        try {
            return typedQuery.getSingleResult();
        } catch (final NoResultException e) {
            return null;
        }
    }

    /**
     * A case-insensitive "like" (similar to Postgres <tt>ilike</tt> operator) using the provided match mode.
     *
     * @param builder the CriteriaBuilder of the corrent context.
     * @param path    the path {@code Expression<String>} with the property name to use in the operation.
     * @param pattern the pattern the pattern to use in the operation.
     * @return the predicate
     */
    public Predicate ilike(final CriteriaBuilder builder, final Expression<String> path, final String pattern) {
        return ilike(builder, path, pattern, MatchMode.EXACT);
    }

    /**
     * A case-insensitive "like" (similar to Postgres <tt>ilike</tt> operator) using the provided match mode.
     *
     * @param builder   the CriteriaBuilder of the corrent context.
     * @param path      the path {@code Expression<String>} with the property name to use in the operation.
     * @param pattern   the pattern the pattern to use in the operation.
     * @param matchMode the exact, by default is used {@code MatchMode.EXACT} .
     * @return the predicate
     */
    public Predicate ilike(final CriteriaBuilder builder, final Expression<String> path,
                           final String pattern, final MatchMode matchMode) {
        if (pattern == null) {
            throw new IllegalArgumentException("Comparison value passed to ilike cannot be null");
        }
        final MatchMode mode = (matchMode == null) ? MatchMode.EXACT : matchMode;

        return builder.like(builder.upper(path), mode.toMatchString(pattern.trim().toUpperCase()));
    }

    /**
     * Builde order by of some expression.
     *
     * @param builder      the builder define the current criteria builder.
     * @param expression   the expression - define the property to order by.
     * @param isDescendent the is descendent - define if is asc or desc, is desc if true, asc otherwise
     * @return the order
     */
    public Order orderBy(final CriteriaBuilder builder, final Expression<?> expression, final boolean isDescendent) {
        if (isDescendent) {
            return builder.desc(expression);

        }
        return builder.asc(expression);
    }

    /**
     * Builde order by of some expression.
     *
     * @param builder    the builder define the current criteria builder.
     * @param expression the expression - define the property to order by.
     * @return the order
     */
    public Order orderBy(final CriteriaBuilder builder, final Expression<?> expression) {
        return orderBy(builder, expression, false);
    }

    /**
     * Cacheable.
     *
     * @param <X>        the generic type
     * @param typedQuery the typed query
     * @return the typed query
     */
    public <X> TypedQuery<X> cacheable(final TypedQuery<X> typedQuery) {
        typedQuery.setHint("org.hibernate.cacheable", Boolean.TRUE);
        // typedQuery.unwrap(org.hibernate.Query.class).setCacheable(true)
        return typedQuery;
    }

    /**
     * Represents an strategy for matching strings using "like".
     */
    public enum MatchMode {

        /**
         * Match the entire string to the pattern. ("pattern")
         */
        EXACT {
            @Override
            public String toMatchString(final String pattern) {
                return pattern;
            }
        },

        /**
         * Match the start of the string to the pattern. ( "pattern%" )
         */
        START {
            @Override
            public String toMatchString(final String pattern) {
                return pattern + '%';
            }
        },

        /**
         * Match the end of the string to the pattern. ("%pattern")
         */
        END {
            @Override
            public String toMatchString(final String pattern) {
                return '%' + pattern;
            }
        },

        /**
         * Match the pattern anywhere in the string. ( "%pattern%" )
         */
        ANYWHERE {
            @Override
            public String toMatchString(final String pattern) {
                return '%' + pattern + '%';
            }
        };

        /**
         * Convert the pattern, by appending/prepending "%".
         *
         * @param pattern The pattern for convert according to the mode
         * @return The converted pattern
         */
        public abstract String toMatchString(String pattern);
    }

}
