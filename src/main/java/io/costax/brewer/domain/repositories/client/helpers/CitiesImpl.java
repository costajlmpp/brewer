package io.costax.brewer.domain.repositories.client.helpers;

import io.costax.brewer.domain.model.client.City;
import io.costax.brewer.domain.model.client.City_;
import io.costax.brewer.domain.model.client.Country;
import io.costax.brewer.domain.model.client.Country_;
import io.costax.brewer.domain.repositories.CriteriaMethods;
import io.costax.brewer.domain.repositories.Strings;
import io.costax.brewer.domain.repositories.client.filters.CityFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class CitiesImpl implements CitiesQueries {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CriteriaMethods helper;

    @Override
    public Page<City> search(final CityFilter filter, final Pageable pageable) {
        return new PageImpl<>(getSearchResult(filter, pageable), pageable, total(filter));
    }

    private long total(CityFilter filter) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();

        final CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        final Root<City> root = cq.from(City.class);

        Predicate[] predicates = toPredicates(filter, cb, root);
        Order[] orders = {};

        return em.createQuery(cq.select(cb.count(root.get(City_.id)))
                .where(predicates).orderBy(orders))
                .getSingleResult();
    }

    private List<City> getSearchResult(final CityFilter filter, final Pageable pageable) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();

        final CriteriaQuery<City> cq = cb.createQuery(City.class);
        final Root<City> root = cq.from(City.class);
        @SuppressWarnings("unchecked") final Join<City, Country> countries = (Join) root.fetch(City_.country, JoinType.LEFT);

        Predicate[] predicates = toPredicates(filter, cb, root);
        List<Order> orders = new ArrayList<>(helper.ordersOf(pageable, cb, root, City_.NAME, City_.ID, City_.COUNTRY));

        if (pageable.getSort() != null && pageable.getSort().getOrderFor("country.name") != null) {
            final Sort.Order orderFor = pageable.getSort().getOrderFor("country.name");
            orders.add(helper.toOrder(cb, countries.get(Country_.name), orderFor.getDirection()));
        }

        return em.createQuery(cq.select(root).distinct(true).where(predicates).orderBy(orders))
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    private Predicate[] toPredicates(final CityFilter filter, final CriteriaBuilder cb, final Root<City> root) {
        List<Predicate> predicates = new ArrayList<>();

        if (Strings.isNotBlank(filter.getName())) {
            predicates.add(helper.ilike(cb, root.get(City_.name), filter.getName(), CriteriaMethods.MatchMode.START));
        }

        if (filter.getCountry() != null) {
            predicates.add(cb.equal(root.get(City_.country), filter.getCountry()));
        }

        return predicates.toArray(new Predicate[0]);
    }

}
