package io.costax.brewer.domain.repositories.sale;

import io.costax.brewer.domain.model.sale.Sale;
import io.costax.brewer.domain.repositories.sale.helpers.SalesQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Sales extends JpaRepository<Sale, Long>, SalesQueries {
}
