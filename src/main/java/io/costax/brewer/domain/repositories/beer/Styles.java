package io.costax.brewer.domain.repositories.beer;

import io.costax.brewer.domain.model.beer.Style;
import io.costax.brewer.domain.repositories.beer.helpers.StylesQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Styles extends JpaRepository<Style, Long>, StylesQueries {

    Optional<Style> findByNameIgnoreCase(String name);
}
