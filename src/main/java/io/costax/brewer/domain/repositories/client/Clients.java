package io.costax.brewer.domain.repositories.client;

import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.repositories.client.helpers.ClientQueries;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface Clients extends JpaRepository<Client, Long>, ClientQueries {

    Optional<Client> findByNif(Integer nif);

    List<Client> findByNameStartingWithIgnoreCaseOrderById(String name);
}
