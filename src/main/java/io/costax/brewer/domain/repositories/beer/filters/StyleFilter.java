package io.costax.brewer.domain.repositories.beer.filters;

public class StyleFilter {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
