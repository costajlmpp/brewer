package io.costax.brewer.domain.repositories.beer.helpers;

import io.costax.brewer.domain.model.beer.Style;
import io.costax.brewer.domain.repositories.CriteriaMethods;
import io.costax.brewer.domain.repositories.beer.filters.StyleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StylesImpl implements StylesQueries {

    private static final Set<String> ORDER_PROPERTIES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("name", "id")));

    @PersistenceContext
    private EntityManager manager;

    private final CriteriaMethods criteriaMethods;

    @Autowired
    StylesImpl(CriteriaMethods criteriaMethods) {
        this.criteriaMethods = criteriaMethods;
    }

    @Override
    public Page<Style> search(StyleFilter filter, Pageable pageable) {
        return new PageImpl<>(getStyles(filter, pageable), pageable, total(filter));
    }

    private List<Style> getStyles(StyleFilter filter, Pageable pageable) {
        final CriteriaBuilder cb = manager.getCriteriaBuilder();
        final CriteriaQuery<Style> cq = cb.createQuery(Style.class);
        final Root<Style> stylesRoot = cq.from(Style.class);

        List<Predicate> predicates = toPredicates(filter, cb, stylesRoot);

        List<Order> orders = criteriaMethods.ordersOf(pageable, cb, stylesRoot, ORDER_PROPERTIES);

        return manager.createQuery(
                cq.select(stylesRoot).distinct(true)
                        .where(predicates.toArray(new Predicate[0]))
                        .orderBy(orders))
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize())
                .getResultList();
    }

    private List<Predicate> toPredicates(StyleFilter filter, CriteriaBuilder cb, Root<Style> stylesRoot) {
        return Optional.ofNullable(filter)
                .map(Stream::of).orElseGet(Stream::empty)
                .map(StyleFilter::getName)
                .filter(Objects::nonNull)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .map(String::toUpperCase)
                .map(s -> cb.like(cb.upper(stylesRoot.get("name")), s + "%"))
                .collect(Collectors.toList());
    }

    private Long total(StyleFilter filter) {
        final CriteriaBuilder cb = manager.getCriteriaBuilder();
        final CriteriaQuery<Long> c = cb.createQuery(Long.class);
        final Root<Style> styleRoot = c.from(Style.class);

        List<Predicate> predicates = toPredicates(filter, cb, styleRoot);

        return manager.createQuery(
                c.select(cb.count(styleRoot.get("id"))).where(predicates.toArray(new Predicate[0]))).getSingleResult();

    }

}
