package io.costax.brewer.domain.repositories.sale.filters;

import java.io.Serializable;

public class SaleFilter implements Serializable {

    private String code;

    private String nif;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(final String nif) {
        this.nif = nif;
    }
}
