package io.costax.brewer.domain.repositories.client.helpers;

import io.costax.brewer.domain.model.client.City;
import io.costax.brewer.domain.repositories.client.filters.CityFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CitiesQueries {

    Page<City> search(CityFilter filter, Pageable pageable);
}
