package io.costax.brewer.domain.repositories.client;

import io.costax.brewer.domain.model.client.Country;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface Countries extends JpaRepository<Country, Long> {

    @Cacheable("all-countries")
    @Query("select c from Country c order by upper(c.name)")
    List<Country> findAll();
}
