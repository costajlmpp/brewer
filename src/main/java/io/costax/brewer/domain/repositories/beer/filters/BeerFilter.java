package io.costax.brewer.domain.repositories.beer.filters;

import io.costax.brewer.domain.model.beer.Flavor;
import io.costax.brewer.domain.model.beer.Origin;
import io.costax.brewer.domain.model.beer.Style;

import java.math.BigDecimal;

public class BeerFilter {

    private String code;
    private String name;
    private Style style;
    private Flavor flavor;
    private Origin origin;
    private BigDecimal valueSince;
    private BigDecimal valueUpTo;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Flavor getFlavor() {
        return flavor;
    }

    public void setFlavor(Flavor flavor) {
        this.flavor = flavor;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public BigDecimal getValueSince() {
        return valueSince;
    }

    public void setValueSince(BigDecimal valueSince) {
        this.valueSince = valueSince;
    }

    public BigDecimal getValueUpTo() {
        return valueUpTo;
    }

    public void setValueUpTo(BigDecimal valueUpTo) {
        this.valueUpTo = valueUpTo;
    }
}
