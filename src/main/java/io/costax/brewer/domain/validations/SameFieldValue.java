package io.costax.brewer.domain.validations;

import io.costax.brewer.domain.validations.validators.SameFieldValueValidator;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = SameFieldValueValidator.class)
@Retention(RUNTIME)
@Target({TYPE})
public @interface SameFieldValue {

    @OverridesAttribute(constraint = Pattern.class, name = "message")
    String message() default "{io.costax.brewer.domain.validation.SameFieldValue.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String field();

    String confirmation();
}
