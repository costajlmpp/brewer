package io.costax.brewer.domain.validations.validators;

import io.costax.brewer.domain.validations.DecimalRange;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class DecimalRangeValidator implements ConstraintValidator<DecimalRange, BigDecimal> {

    private DecimalRange decimalRange;

    public void initialize(DecimalRange decimalRange) {
        this.decimalRange = decimalRange;
    }

    public boolean isValid(BigDecimal obj, ConstraintValidatorContext context) {
        return validMin(obj) && validMax(obj);
    }

    private boolean validMax(BigDecimal value) {
        final int i = value.compareTo(bigDecimalOf(decimalRange.max()));

        if (!decimalRange.maxInclusive()) {
            return i < 0;
        }

        return i <= 0;
    }

    private boolean validMin(BigDecimal value) {
        final int i = value.compareTo(bigDecimalOf(decimalRange.min()));

        if (!decimalRange.minInclusive()) {
            return i > 0;
        }

        return i >= 0;
    }

    private BigDecimal bigDecimalOf(String value) {
        return BigDecimal.valueOf(Double.parseDouble(value));
    }
}
