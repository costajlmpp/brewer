package io.costax.brewer.domain.validations;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Pattern(regexp = "([A-Z]{2}\\d{4})?")
@Constraint(validatedBy = {})
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
public @interface BeerCode {

    @OverridesAttribute(constraint = Pattern.class, name = "message")
    String message() default "{io.costax.brewer.domain.validation.BeerCode.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
