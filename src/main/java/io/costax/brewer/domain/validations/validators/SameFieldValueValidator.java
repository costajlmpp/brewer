package io.costax.brewer.domain.validations.validators;

import io.costax.brewer.domain.validations.SameFieldValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.Objects;

public class SameFieldValueValidator implements ConstraintValidator<SameFieldValue, Object> {
    private String fieldName;
    private String confirmationFieldName;

    public void initialize(SameFieldValue constraint) {
        this.fieldName = constraint.field();
        this.confirmationFieldName = constraint.confirmation();
    }

    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        boolean isvalid = false;
        try {

            /* we could use the apache dependency
             * commons-beanutils:commons-beanutils:1.9.2
             * but it is so simple, that it does not find benefit of having more dependence just to do this
             */
            final Class<?> aClass = obj.getClass();

            final Field field1 = aClass.getDeclaredField(fieldName);
            field1.setAccessible(true);
            final Field field2 = aClass.getDeclaredField(confirmationFieldName);
            field2.setAccessible(true);

            final Object value1 = field1.get(obj);
            final Object value2 = field2.get(obj);

            isvalid = Objects.equals(value1, value2);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Some error trying recovery the fields", e);
        }

        if (!isvalid) {
            final String messageTemplate = context.getDefaultConstraintMessageTemplate();
            final ConstraintValidatorContext.ConstraintViolationBuilder builder = context.buildConstraintViolationWithTemplate(messageTemplate);
            builder.addPropertyNode(confirmationFieldName).addConstraintViolation();
        }

        return isvalid;
    }
}
