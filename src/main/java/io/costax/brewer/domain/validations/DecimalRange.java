package io.costax.brewer.domain.validations;

import io.costax.brewer.domain.validations.validators.DecimalRangeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Constraint(validatedBy = DecimalRangeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
public @interface DecimalRange {

    String min() default "0.01";

    String max() default "9999999.99";

    boolean minInclusive() default true;

    boolean maxInclusive() default true;

    String message() default "{io.costax.brewer.domain.validation.DecimalRange.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
