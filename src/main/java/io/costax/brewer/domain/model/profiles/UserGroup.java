package io.costax.brewer.domain.model.profiles;

import com.google.common.collect.ImmutableSet;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Immutable
@Table(name = "user_group")
public class UserGroup {

    @Id
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private long id;

    @NotBlank(message = "{usergroup.name.NotBlank.message}")
    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_group_permission",
            joinColumns = @JoinColumn(name = "user_group_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<Permission> permissions = new HashSet<>();

    public static UserGroup withId(Long id) {
        final UserGroup userGroup = new UserGroup();
        userGroup.id = id;
        return userGroup;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Permission> getPermissions() {
        return ImmutableSet.copyOf(permissions);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof UserGroup)) return false;
        final UserGroup userGroup = (UserGroup) o;
        return this.id == userGroup.id;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(this.id);
    }
}
