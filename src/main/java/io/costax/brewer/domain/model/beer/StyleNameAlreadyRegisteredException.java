package io.costax.brewer.domain.model.beer;

public class StyleNameAlreadyRegisteredException extends RuntimeException {

    private final String name;

    public StyleNameAlreadyRegisteredException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDetailMessage() {
        return "Style with the name '" + this.name + "' already registered";
    }
}
