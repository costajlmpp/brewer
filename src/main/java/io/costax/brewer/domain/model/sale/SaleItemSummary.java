package io.costax.brewer.domain.model.sale;

import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.model.beer.Image;
import io.costax.brewer.domain.model.beer.Origin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Optional;

public class SaleItemSummary implements Serializable {
    private final BigDecimal total;
    private Long saleId;
    private Long beerId;
    private String beerCode;
    private String beerName;
    private String beerImage;
    private BigDecimal beerValue = BigDecimal.ONE;
    private String beerOriginDescription;
    private int qty;

    public SaleItemSummary(final Long saleId, final Beer beer, final int qty, final BigDecimal total) {
        this.saleId = saleId;
        this.qty = qty;
        this.total = total;

        if (beer != null) {
            this.beerId = beer.getId();
            this.beerCode = beer.getCode();
            this.beerName = beer.getName();
            this.beerValue = beer.getValue();
            this.beerOriginDescription = Optional.ofNullable(beer.getOrigin()).map(Origin::getDescription).orElse("");
            this.beerImage = Optional.ofNullable(beer.getImage()).map(Image::getName).orElse(null);
        }
    }

    protected static SaleItemSummary of(final Long saleId,
                                        final Beer beer,
                                        final int qty,
                                        final BigDecimal total) {
        return new SaleItemSummary(saleId, beer, qty, total);
    }

    public Long getBeerId() {
        return beerId;
    }

    public Long getSaleId() {
        return saleId;
    }

    public String getBeerCode() {
        return beerCode;
    }

    public String getBeerName() {
        return beerName;
    }

    public BigDecimal getBeerValue() {
        return beerValue;
    }

    public int getQty() {
        return qty;
    }

    public String getBeerImage() {
        return beerImage;
    }

    public String getBeerOriginDescription() {
        return beerOriginDescription;
    }

    public BigDecimal getTotal() {
        return total;
    }

}
