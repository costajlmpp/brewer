package io.costax.brewer.domain.model.beer;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

public class BeerSummary {

    private Long id;
    private String code;
    private String name;
    private String origin;
    private BigDecimal value;
    private String imageName;

    public BeerSummary() {
    }

    public BeerSummary(final Long id,
                       final String code,
                       final String name,
                       final Origin origin,
                       final BigDecimal value,
                       final String imageName) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.origin = Optional.ofNullable(origin).map(Origin::getDescription).orElse(null);
        this.value = value;
        this.imageName = imageName;
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getImageName() {
        return imageName;
    }

    public String getOrigin() {
        return origin;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof BeerSummary)) return false;
        final BeerSummary that = (BeerSummary) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "BeerSummary{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
