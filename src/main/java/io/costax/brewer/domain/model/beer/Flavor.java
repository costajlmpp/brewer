package io.costax.brewer.domain.model.beer;

public enum Flavor {

    SOFT_HOPPY_AROMA("soft hoppy aroma"),
    TOASTED_BARLEY_AROMA("toasted barley aroma"),
    INTENSE_LAVOUR_AND_AROMA("intense flavour and aroma");

    private final String description;

    Flavor(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
