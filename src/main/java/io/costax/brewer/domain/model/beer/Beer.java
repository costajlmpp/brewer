package io.costax.brewer.domain.model.beer;

import io.costax.brewer.domain.validations.BeerCode;
import io.costax.brewer.domain.validations.DecimalRange;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

@Entity
@Table(name = "beer")
public class Beer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @NotBlank(message = "{beer.code.NotBlank.message}")
    @Size(max = 100, message = "{beer.code.Size.message}")
    @BeerCode
    private String code;

    @NotBlank(message = "{beer.name.NotBlank.message}")
    @Size(max = 80, message = "{beer.name.Size.message}")
    private String name;

    @Size(max = 250, message = "{beer.description.Size.message}")
    @NotBlank(message = "{beer.description.NotBlank.message}")
    private String description;

    @NotNull(message = "{beer.value.NotNull.message}")
    @DecimalRange(message = "{beer.value.DecimalRange.message}")
    private BigDecimal value = BigDecimal.ONE;

    @NotNull(message = "{beer.alcoholicStrength.NotNull.message}")
    @Range(max = 100L, message = "beer.alcoholicStrength.Range.message")
    @Column(name = "alcoholic_strength")
    private BigDecimal alcoholicStrength = BigDecimal.valueOf(15.0D);

    @NotNull(message = "{beer.commission.NotNull.message}")
    @DecimalRange(max = "100.0", message = "{beer.commission.DecimalRange.message}")
    private BigDecimal commission = BigDecimal.valueOf(0.1D);

    @NotNull(message = "{beer.style.NotNull.message}")
    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @NotNull(message = "{beer.flavor.NotNull.message}")
    @Enumerated(EnumType.STRING)
    private Flavor flavor;

    @NotNull(message = "{beer.origin.NotNull.message}")
    @Enumerated(EnumType.STRING)
    private Origin origin;

    @Embedded
    @AttributeOverride(name = "name", column = @Column(name = "image_name"))
    @AttributeOverride(name = "contentType", column = @Column(name = "image_content_type"))
    private Image image;

    public Beer() {
    }

    private Beer(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static Beer of(String code, String name) {
        return new Beer(code, name);
    }

    public static Beer of(String code, String name, BigDecimal value) {
        Beer beer = new Beer(code, name);
        beer.value = value;

        return beer;
    }

    public static Beer empty() {
        return new Beer();
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public BigDecimal getAlcoholicStrength() {
        return alcoholicStrength;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public Flavor getFlavor() {
        return flavor;
    }

    public Origin getOrigin() {
        return origin;
    }

    public Style getStyle() {
        return style;
    }

    public Image getImage() {
        return image;
    }

    public Optional<String> getImageName() {
        return Optional.ofNullable(image)
                .map(Image::getName)
                .map(String::trim)
                .filter(s -> !s.isEmpty());
    }

    public String getImageNameOrMock() {
        return this.getImageName().orElse("beer-mock.png");
    }

    public boolean hasImageDefined() {
        return this.image != null && image.isDefined();
    }


    //    public void setCode(String code) {
//        this.code = code;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return id != null && Objects.equals(id, beer.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public void removeImage() {
        this.image = null;
    }
}
