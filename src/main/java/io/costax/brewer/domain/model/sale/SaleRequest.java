package io.costax.brewer.domain.model.sale;

import java.io.Serializable;
import java.math.BigDecimal;

public class SaleRequest implements Serializable {

    private Long client;
    private BigDecimal portsValue = BigDecimal.ZERO;
    private BigDecimal discountValue = BigDecimal.ZERO;


    public Long getClient() {
        return client;
    }

    public void setClient(final Long client) {
        this.client = client;
    }

    public BigDecimal getPortsValue() {
        return portsValue;
    }

    public void setPortsValue(final BigDecimal portsValue) {
        this.portsValue = portsValue;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(final BigDecimal discountValue) {
        this.discountValue = discountValue;
    }
}
