package io.costax.brewer.domain.model.beer;

public enum Origin {

    HOMEMADE("Homemade"),
    MANUFACTURED("Manufactured");

    private final String description;

    Origin(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
