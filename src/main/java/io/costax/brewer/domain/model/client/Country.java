package io.costax.brewer.domain.model.client;


import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @NotBlank
    @Size(max = 20)
    private String acronym;

    @NotBlank
    @Size(max = 150)
    private String name;

    public Country() {
    }

    private Country(@NotBlank @Size(max = 20) String acronym, @NotBlank @Size(max = 150) String name) {
        this.acronym = acronym;
        this.name = name;
    }

    public static Country of(String acronym, String name) {
        return new Country(acronym, name);
    }

    public static Country of(Long id) {
        Country country = new Country();
        country.id = id;
        return country;
    }



    public Long getId() {
        return id;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Objects.equals(id, country.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
