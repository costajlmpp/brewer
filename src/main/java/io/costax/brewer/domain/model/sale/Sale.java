package io.costax.brewer.domain.model.sale;

import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.profiles.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table(name = "sale")
public class Sale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @Column(name = "total", nullable = false)
    private BigDecimal total = BigDecimal.ZERO;

    @NotNull
    @PositiveOrZero
    @Column(name = "ports_value", nullable = false)
    private BigDecimal portsValue = BigDecimal.ZERO;

    @NotNull
    @PositiveOrZero
    @Column(name = "discount_value", nullable = false)
    private BigDecimal discountValue = BigDecimal.ZERO;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "state", nullable = false)
    private State state = State.NEW;

    @OneToMany(mappedBy = "sale", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SalesItem> items = new LinkedHashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    @Column(name = "create_at", nullable = false, updatable = false)
    private LocalDateTime createAt;

    protected Sale() {
    }

    private Sale(final Client client, final User user) {
        this.client = client;
        this.user = user;
    }

    public static Sale empty() {
        return new Sale();
    }

    public static Sale empty(Client client, User user) {
        if (client == null) throw new IllegalArgumentException("The sale client can't be null");

        return new Sale(client, user);
    }

    public void setClient(final Client client) {
        this.client = client;
    }

    public Sale add(Beer beer, int qty) {
        validateIfAllowAddOrModifyItems();

        if (beer == null) throw new IllegalArgumentException("Can't add a null beer to a sale");
        if (qty <= 0) throw new IllegalArgumentException("The qty must be greater than \"ZERO\"");

        Optional<SalesItem> existing = getItemByBeerId(beer);

        if (existing.isPresent()) {
            SalesItem salesItem = existing.get();
            salesItem.plusQty(qty);
        } else {

            SalesItem item = SalesItem.of(qty, beer, this);
            this.items.add(item);
        }

        this.total = this.calculate();
        return this;
    }

    private BigDecimal calculate() {
        BigDecimal itemsTotal = this.items.stream()
                .map(SalesItem::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return itemsTotal.add(this.portsValue).subtract(this.discountValue);
    }

    private Optional<SalesItem> getItemByBeerId(final Beer beer) {
        return this.items.stream()
                .filter(si -> Objects.equals(beer, si.getBeer()))
                .findAny();
    }

    public Long getId() {
        return id;
    }

    public Set<SalesItem> getItems() {
        return Collections.unmodifiableSet(items);
    }

    public BigDecimal getTotal() {
        return total;
    }

    public int size() {
        return this.items.size();
    }

    public void setQty(final Beer beer, final int qty) {
        validateIfAllowAddOrModifyItems();

        SalesItem item = getItemByBeerId(beer).orElse(null);

        if (item == null) {
            throw new IllegalArgumentException(
                    String.format("Can't define the qty of nonexistent beer in a sale. The sale do not contain the beer '%s'.", beer));
        }

        if (qty == 0) {
            removeItem(item);
        } else {
            item.setQty(qty);
        }

        recalculate();
    }

    private void removeItem(SalesItem salesItem) {
        this.items.remove(salesItem);
        salesItem.setSale(null);
    }

    private void validateIfAllowAddOrModifyItems() {
        if (!this.state.allowAddOrModifyItems()) {
            throw new IllegalStateException(
                    String.format("The current sale [%s] don't allow modify items", this));
        }
    }

    private void recalculate() {
        this.total = calculate();
    }

    public boolean containsBeer(final Beer beer) {
        return this.items.stream().map(SalesItem::getBeer).anyMatch(b -> b.equals(beer));
    }

    public void remove(final Beer beer) {
        validateIfAllowAddOrModifyItems();
        SalesItem salesItem = getItemByBeerId(beer).orElse(null);

        if (salesItem != null) {
            removeItem(salesItem);
            recalculate();
        }
    }


    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public Client getClient() {
        return client;
    }

    public BigDecimal getPortsValue() {
        return portsValue;
    }

    public void setPortsValue(final BigDecimal portsValue) {
        validateIfAllowAddOrModifyItems();
        this.portsValue = portsValue;
        calculate();
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(final BigDecimal discountValue) {
        validateIfAllowAddOrModifyItems();
        this.discountValue = discountValue;
        calculate();
    }

    @PrePersist
    void defineCreateAt() {
        this.createAt = LocalDateTime.now();
    }

    public enum State {
        NEW(true),
        SUBMITTED(false),
        INVOICED(false),
        CLOSED(false);

        private final boolean allowAddOrModifyItems;

        State(final boolean allowAddOrModifyItems) {
            this.allowAddOrModifyItems = allowAddOrModifyItems;
        }

        public boolean allowAddOrModifyItems() {
            return allowAddOrModifyItems;
        }
    }
}
