package io.costax.brewer.domain.model.beer;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "style")
public class Style implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{style.name.NotBlank.message}")
    @Size(min = 1, max = 50, message = "{style.name.Size.message}")
    private String name;

    public Style() {
    }

    private Style(@NotBlank @Size(max = 50) String name) {
        this.name = name;
    }

    public static Style of(@NotBlank @Size(max = 50) String name) {
        return new Style(name);
    }

    public static Style of(Long id) {
        final Style style = new Style();
        style.id = id;
        return style;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Style style = (Style) o;
        return id != null && Objects.equals(id, style.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Style{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
