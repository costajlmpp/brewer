package io.costax.brewer.domain.model.client;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class Address implements Serializable {

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Transient
    private Country country;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "street_address")
    private String streetAddress;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCountryAcronymAndCityName() {
        if (this.city == null) {
            return "";
        }
        return city.getCountryAcronymAndCityName();
    }
}
