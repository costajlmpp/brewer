package io.costax.brewer.domain.model.sale;

import io.costax.brewer.domain.model.beer.Beer;
import org.hibernate.annotations.LazyGroup;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

@Entity
@Table(name = "sales_item")
public class SalesItem {

    @EmbeddedId
    private SalesItemPk id;

    @Column(name = "qty", nullable = false)
    private int qty;

    @Column(name = "unit_value", nullable = false)
    private BigDecimal unitValue;

    @LazyGroup("sales_item_beer")
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("beerId")
    private Beer beer;

    @ManyToOne(fetch = FetchType.LAZY)
    @LazyGroup("sales_item_sale")
    @MapsId("saleId")
    private Sale sale;

    protected SalesItem() {
    }

    private SalesItem(final int qty,
                      final Beer beer,
                      final Sale sale) {
        this.id = SalesItemPk.of(sale.getId(), beer.getId());
        this.qty = qty;
        this.unitValue = beer.getValue();
        this.beer = beer;
        this.sale = sale;
    }

    public static SalesItem of(final int qty,
                               final Beer beer,
                               final Sale sale) {
        if (beer == null) throw new IllegalArgumentException("The beer can't be null in the sales item");
        if (sale == null) throw new IllegalArgumentException("The sale can't be null in the sales item");

        return new SalesItem(qty, beer, sale);
    }

    protected void plusQty(final int qty) {
        this.qty += qty;
    }

    public Beer getBeer() {
        return beer;
    }

    public SalesItemPk getId() {
        return id;
    }

    public int getQty() {
        return qty;
    }

    protected void setQty(final int qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public Sale getSale() {
        return sale;
    }

    protected void setSale(final Sale o) {
        this.sale = o;
    }

    public BigDecimal getValue() {
        return this.unitValue.multiply(BigDecimal.valueOf(this.qty));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof SalesItem)) return false;
        final SalesItem salesItem = (SalesItem) o;
        return Objects.equals(beer, salesItem.beer) &&
                Objects.equals(sale, salesItem.sale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(beer, sale);
    }

    @SuppressWarnings("ConstantConditions")
    public SaleItemSummary toSummary() {
        if (this.beer == null) throw new IllegalStateException("Can't exist salesItem without associated beer");

        Long saleId = Optional.ofNullable(sale).map(Sale::getId).orElse(null);

        return SaleItemSummary.of(saleId, this.beer, this.qty, getValue());
    }
}
