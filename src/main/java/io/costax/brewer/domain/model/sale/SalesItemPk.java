package io.costax.brewer.domain.model.sale;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SalesItemPk implements Serializable {

    @Column(name = "sale_id", nullable = false, updatable = false)
    private Long saleId;
    @Column(name = "beer_id", nullable = false, updatable = false)
    private Long beerId;

    protected SalesItemPk() {
    }

    private SalesItemPk(final Long saleId, final Long beerId) {
        this.saleId = saleId;
        this.beerId = beerId;
    }

    public static SalesItemPk of(final Long saleId, final Long beerId) {
        return new SalesItemPk(saleId, beerId);
    }

    public Long getSaleId() {
        return saleId;
    }

    public Long getBeerId() {
        return beerId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof SalesItemPk)) return false;
        final SalesItemPk that = (SalesItemPk) o;
        return Objects.equals(saleId, that.saleId) &&
                Objects.equals(beerId, that.beerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleId, beerId);
    }

    @Override
    public String toString() {
        return "SalesItemPk{" +
                "saleId=" + saleId +
                ", beerId=" + beerId +
                '}';
    }
}
