package io.costax.brewer.domain.model.profiles;

public class EmailAlreadyRegistedException extends RuntimeException {
    private final String email;

    public EmailAlreadyRegistedException(String email) {
        this.email = email;
    }

    public String getDetailMessage() {
        return String.format("The email '%s' is already in register", email);
    }
}
