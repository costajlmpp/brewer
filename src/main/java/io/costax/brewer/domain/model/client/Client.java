package io.costax.brewer.domain.model.client;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "client")
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{client.name.NotBlank.message}")
    @Size(max = 500, message = "{client.name.Size.message}")
    private String name;

    @Range(min = 100000000, max = 999999999, message = "{client.nif.Size.message}")
    private Integer nif;

    @Size(min = 9, max = 20, message = "{client.phone.Size.message}")
    private String phone;

    @Email
    @Size(max = 150)
    private String email;

    @Embedded
    private Address address;

    public Client() {
    }

    private Client(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNif() {
        return nif;
    }

    public void setNif(Integer nif) {
        this.nif = nif;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCountryAcronymAndCityName() {
        if (this.address == null) {
            return "";
        }

        return this.address.getCountryAcronymAndCityName();
    }

    public static Client of(Long id) {
        return new Client(id);
    }
}
