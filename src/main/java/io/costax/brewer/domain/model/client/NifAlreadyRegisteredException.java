package io.costax.brewer.domain.model.client;

public class NifAlreadyRegisteredException extends RuntimeException {

    private final Integer nif;

    public NifAlreadyRegisteredException(Integer nif) {
        this.nif = nif;
    }

    public String getDetailMessage() {
        return String.format("The nif '%s' is already in register", nif);
    }
}
