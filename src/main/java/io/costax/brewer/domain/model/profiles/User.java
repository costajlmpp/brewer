package io.costax.brewer.domain.model.profiles;

import com.google.common.collect.ImmutableSet;
import io.costax.brewer.domain.validations.SameFieldValue;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@SameFieldValue(field = "password", confirmation = "passwordConfirmation", message = "{user.password.SameFieldValue.message}")
@Entity
@Table(name = "user")
public class User implements Persistable<Long>, java.util.Formattable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @NotBlank(message = "{user.name.NotBlank.message}")
    @Column(name = "name", nullable = false)
    private String name;

    @NaturalId
    @NotBlank(message = "{user.email.NotBlank.message}")
    @Email(message = "{user.email.Email.message}")
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    private boolean enable = true;

    @Column(name = "assigned_at")
    private LocalDate assignedAt;

    @NotNull(message = "{user.groups.NotBlank.message}")
    @NotEmpty(message = "{user.groups.NotEmpty.message}")
    @ManyToMany
    // we don't need the cascade because when we persist the the user all the groups are already persisted
    //@ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_group_user",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "user_group_id"))
    private Set<UserGroup> groups = new HashSet<>();

    @Transient
    private String passwordConfirmation;

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Set<UserGroup> getGroups() {
        return Collections.unmodifiableSet(groups);
    }

    public void setGroups(Set<UserGroup> groups) {
        this.groups.retainAll(groups);
        this.groups.addAll(groups);
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public LocalDate getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(LocalDate assignedAt) {
        this.assignedAt = assignedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getId() != null && Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Transient
    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Transient
    public Set<Permission> getPermissions() {
        return this.groups.stream()
                .flatMap(g -> g.getPermissions().stream())
                .collect(Collectors.collectingAndThen(Collectors.toSet(), ImmutableSet::copyOf));
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public void formatTo(final Formatter formatter, final int flags, final int width, final int precision) {
        formatter.format("%s", this.name);
    }
}
