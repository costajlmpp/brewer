package io.costax.brewer.domain.model.client;

import java.io.Serializable;
import java.util.Objects;

public class ClientSummary implements Serializable {

    private Long id;
    private String name;
    private String phone;

    public ClientSummary() {
    }

    public ClientSummary(final Long id, final String name, final String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientSummary)) return false;
        final ClientSummary that = (ClientSummary) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
