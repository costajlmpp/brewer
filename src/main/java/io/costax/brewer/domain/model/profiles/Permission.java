package io.costax.brewer.domain.model.profiles;

import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Immutable
@Table(name = "permission")
public class Permission {

    @Id
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private long id;

    @NotBlank(message = "{permission.name.NotBlank.message}")
    @Column(name = "name", nullable = false)
    private String name;

    public long getId() {
        return id;
    }

    protected void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Permission)) return false;
        final Permission that = (Permission) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }
}
