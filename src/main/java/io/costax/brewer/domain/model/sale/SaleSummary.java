package io.costax.brewer.domain.model.sale;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class SaleSummary implements Serializable {

    private Long id;
    private String clientName;
    private LocalDateTime createdAt;
    private BigDecimal total;
    private Sale.State status;
    private String sellerName;

    protected SaleSummary() {
    }

    public SaleSummary(final Long id,
                       final String clientName,
                       final LocalDateTime createAt,
                       final BigDecimal total,
                       final Sale.State status,
                       final String sellerName) {
        this.id = id;
        this.clientName = clientName;
        this.createdAt = createAt;
        this.total = total;
        this.status = status;
        this.sellerName = sellerName;
    }

    public Long getId() {
        return id;
    }

    public String getClientName() {
        return clientName;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public Sale.State getStatus() {
        return status;
    }

    public String getSellerName() {
        return sellerName;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof SaleSummary)) return false;
        final SaleSummary that = (SaleSummary) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "SaleSummary{" +
                "id=" + id +
                ", clientName='" + clientName + '\'' +
                ", createdAt=" + createdAt +
                ", total=" + total +
                ", status=" + status +
                ", sellerName='" + sellerName + '\'' +
                '}';
    }
}