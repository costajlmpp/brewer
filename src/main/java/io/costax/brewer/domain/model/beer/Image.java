package io.costax.brewer.domain.model.beer;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Image implements Serializable {

    private String name;
    private String contentType;

    public Image() {
    }

    private Image(String name, String contentType) {
        this.name = name;
        this.contentType = contentType;
    }

    public static Image of(String name, String contentType) {
        return new Image(name, contentType);
    }

    public static Image emptyImage() {
        return new Image();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean isDefined() {
        return this.name != null && !this.name.trim().isEmpty();
    }
}
