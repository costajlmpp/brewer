package io.costax.brewer.controllers;

import io.costax.brewer.application.BeerService;
import io.costax.brewer.application.Logger;
import io.costax.brewer.controllers.page.PageWrapper;
import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.model.beer.BeerSummary;
import io.costax.brewer.domain.model.beer.Flavor;
import io.costax.brewer.domain.model.beer.Origin;
import io.costax.brewer.domain.repositories.beer.Beers;
import io.costax.brewer.domain.repositories.beer.Styles;
import io.costax.brewer.domain.repositories.beer.filters.BeerFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static io.costax.brewer.controllers.Messages.SUCCESS_MESSAGE;

@Controller
public class BeersController {

    private final Logger logger;
    private final Styles styles;
    private final BeerService beerService;
    private final Beers beers;

    @Autowired
    public BeersController(Styles styles,
                           BeerService beerService,
                           Beers beers,
                           Logger logger) {
        this.styles = styles;
        this.beerService = beerService;
        this.beers = beers;
        this.logger = logger;
    }

    @GetMapping("/beers")
    public ModelAndView search(BeerFilter filter,
                               BindingResult result,
                               @PageableDefault(size = 5) Pageable pageable,
                               HttpServletRequest httpServletRequest) {

        ModelAndView mv = new ModelAndView(Views.BEER_SEARCH);

        mv.addObject("filter", filter);
        mv.addObject("styles", styles.findAll());
        mv.addObject("flavors", Flavor.values());
        mv.addObject("origins", Origin.values());

        Page<Beer> page = beers.search(filter, pageable);

        PageWrapper<Beer> pageWrapper = new PageWrapper<>(page, httpServletRequest);

        mv.addObject("page", pageWrapper);

        return mv;
    }

    @RequestMapping(value = "/beers/new")
    public ModelAndView toCreateNew(Beer beer) {

        logger.debug("going to new Beers");

        // Spring is clever enough to know that if we are putting the Beer parameter in the method declaration
        // we should also want to have it in the template renderization.
        // if no beer instance is present in the invocation then the spring will instantiate a new one.
        // So, we do not need to use the model implicitly to implement this behavior:
        // model.addAttribute(beer));
        // Because Spring will make the invocation of this instruction through behind the scenes

        final ModelAndView modelAndView = new ModelAndView(Views.BEER_NEW);

        modelAndView.addObject("beer", beer);
        modelAndView.addObject("styles", styles.findAll());
        modelAndView.addObject("flavors", Flavor.values());
        modelAndView.addObject("origins", Origin.values());

        return modelAndView;

    }

    @RequestMapping(value = "/beers/new", method = RequestMethod.POST)
    public ModelAndView registerNewBeer(@Valid Beer beer,
                                        BindingResult bindingResult,
                                        // we can use the model in alternative to the ModelAndView
                                        //Model model,
                                        RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return toCreateNew(beer);
        }

        beerService.save(beer);

        redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, "Beer added successful!!!");

        return new ModelAndView("redirect:/beers/new");
    }

    /**
     * Get The BeerSummary List as json representation.
     *
     * @param codeOrName beer code or name
     * @return list of BeerSummary
     */
    @RequestMapping(
            value = "/beers/summary",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<BeerSummary> search(String codeOrName) {
        return beers.searchSummaryByCodeOrName(codeOrName);
    }

//    @GetMapping
//    public ModelAndView search() {
//        ModelAndView mv = new ModelAndView("beers/new-beer");
//        return mv;
//    }

    /**
     * The @InitBinder annotated methods will get called on each HTTP request if we don't specify the 'value' element of this annotation.
     * <p>
     * Each time this method is called a new instance of WebDataBinder is passed to it.
     * <p>
     * To be more specific about which objects our InitBinder method applies to,
     * we can supply 'value' element of the annotation @InitBinder.
     * The 'value' element is a single or multiple names of command/form attributes and/or request
     * parameters that this init-binder method is supposed to apply to.
     * <p>
     * for more details we can see the blog post: https://www.logicbig.com/tutorials/spring-framework/spring-web-mvc/spring-custom-property-editor.html
     *
     * @param binder the binder configuration object
     */
    @InitBinder("beer")
    public void configureBinder(WebDataBinder binder) {
        binder.initDirectFieldAccess();
    }
}
