package io.costax.brewer.controllers.thymeleaf.processors;

import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

import javax.servlet.http.HttpServletRequest;

public class ActiveMenuItemAttributeTagProcessor extends AbstractAttributeTagProcessor {

    private static final String ATTRIBUTE_NAME = "activeOn";
    private static final int PRECEDENCE = 1000;

    public ActiveMenuItemAttributeTagProcessor(final String dialectPrefix) {
        super(TemplateMode.HTML,
                dialectPrefix,
                null,
                false,
                ATTRIBUTE_NAME,
                true, PRECEDENCE,
                true);
    }

    @Override
    protected void doProcess(final ITemplateContext context,
                             final IProcessableElementTag tag,
                             final AttributeName attributeName,
                             final String attributeValue,
                             final IElementTagStructureHandler structureHandler) {

        IEngineConfiguration configuration = context.getConfiguration();
        IStandardExpressionParser parser = StandardExpressions.getExpressionParser(configuration);
        IStandardExpression expression = parser.parseExpression(context, attributeValue);
        String menu = (String) expression.execute(context);

        HttpServletRequest request = ((IWebContext) context).getRequest();
        String uri = request.getRequestURI();

        // we could also use startsWith method
        if (uri.matches(menu)) {
            String cssClasses = tag.getAttributeValue("class");
            structureHandler.setAttribute("class", cssClasses + " is-active");
        }

    }
}
