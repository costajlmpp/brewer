package io.costax.brewer.controllers.thymeleaf;

import io.costax.brewer.controllers.thymeleaf.processors.*;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import java.util.HashSet;
import java.util.Set;

public class BrewerDialect extends AbstractProcessorDialect {

    public BrewerDialect() {
        super("Custom Brewer Thymeleaf Dialect", "brewer", StandardDialect.PROCESSOR_PRECEDENCE);
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<>();
        processors.add(new SortableElementTagProcessor(dialectPrefix));
        processors.add(new PaginationElementTagProcessor(dialectPrefix));
        processors.add(new ClassForErrorAttributeTagProcessor(dialectPrefix));
        processors.add(new MessageElementTagProcessor(dialectPrefix));
        processors.add(new ActiveMenuItemAttributeTagProcessor(dialectPrefix));
        return processors;
    }
}
