package io.costax.brewer.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PingController {

    @RequestMapping("/ping/hello")
    public String ping() {
        return "ping";
    }
}
