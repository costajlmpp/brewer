package io.costax.brewer.controllers;


import io.costax.brewer.application.SaleService;
import io.costax.brewer.application.SaleValidator;
import io.costax.brewer.controllers.page.PageWrapper;
import io.costax.brewer.controllers.sessionsale.SessionSaleBuilder;
import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.sale.Sale;
import io.costax.brewer.domain.model.sale.SaleItemSummary;
import io.costax.brewer.domain.model.sale.SaleRequest;
import io.costax.brewer.domain.model.sale.SaleSummary;
import io.costax.brewer.domain.repositories.beer.Beers;
import io.costax.brewer.domain.repositories.sale.Sales;
import io.costax.brewer.domain.repositories.sale.filters.SaleFilter;
import io.costax.brewer.security.AppUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static io.costax.brewer.controllers.Messages.SUCCESS_MESSAGE;

@Controller
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    Beers beers;

    @Autowired
    SessionSaleBuilder sessionSaleBuilder;

    @Autowired
    SaleService saleService;

    @Autowired
    Sales sales;

    @Autowired
    SaleValidator saleValidator;

    @GetMapping
    public ModelAndView search(SaleFilter filter, @PageableDefault(size = 3) Pageable pageable, HttpServletRequest httpServletRequest) {
        ModelAndView mv = new ModelAndView("/sales/sales-search");

        mv.addObject("filter", filter);
        mv.addObject("salesStatus", Sale.State.values());

        Page<SaleSummary> page = sales.search(filter, pageable);

        PageWrapper<SaleSummary> paginaWrapper = new PageWrapper<>(page, httpServletRequest);
        mv.addObject("page", paginaWrapper);

        return mv;
    }


    @GetMapping("/new")
    public ModelAndView goToNew() {
        ModelAndView mv = new ModelAndView("sales/sale");

        Sale sale = this.sessionSaleBuilder.get();
        mv.addObject("sale", sale);

        return mv;
    }


    @PostMapping("/new")
    //@RequestMapping(value = "/new", method = RequestMethod.POST)
    public ModelAndView salvar(SaleRequest saleRequest,
                               RedirectAttributes redirectAttributes,
                               BindingResult bindingResult,
                               @AuthenticationPrincipal AppUserDetails userSession) {
        Sale sale = sessionSaleBuilder.withClient(Client.of(saleRequest.getClient()))
                .withPortsValue(saleRequest.getPortsValue())
                .withDiscountValue(saleRequest.getDiscountValue())
                .get();

        saleValidator.validate(sale, bindingResult);

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, "Sale added successful!!!");

            return this.goToNew();
        }

        saleService.create(userSession.getUser(), sale);

        redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, "Sale added successful!!!");
        return new ModelAndView("redirect:/sales/new");
    }

    @PostMapping("/item")
    public ModelAndView addItem(Long beerId) {
        Beer one = beers.findOne(beerId);
        sessionSaleBuilder.addItem(one, 1);

        return getModelAndViewSalesItemsTable();
    }

    @PutMapping("/item/{beerId}")
    public ModelAndView changeQtyItem(@PathVariable Long beerId, Integer qty) {
        Beer beer = beers.findOne(beerId);

        this.sessionSaleBuilder.changeQty(beer, qty);

        return getModelAndViewSalesItemsTable();
    }

    /**
     * Receiving the Beer instance as Parameter is only possible because we have
     * the following configuration in the WebConfigurator:
     *
     * <code>
     *
     * @param beer that must bee removed
     * @return return all SaleItemSummary in the Sale
     * @Bean public DomainClassConverter<FormattingConversionService> domainClassConverter() {
     * return new DomainClassConverter<FormattingConversionService>(mvcConversionService());
     * }
     * </code>
     * <p>
     * Without the previus configuration we only can use the next signature
     *
     * <code>
     * public ModelAndView removeItem(@PathVariable Long beerId) {
     * </code>
     */
    @DeleteMapping("/item/{beerId}")
    public ModelAndView removeItem(@PathVariable("beerId") Beer beer) {
        //Beer beer = beers.findOne(beerId);

        this.sessionSaleBuilder.remove(beer);

        return getModelAndViewSalesItemsTable();
    }

    private ModelAndView getModelAndViewSalesItemsTable() {
        List<SaleItemSummary> items = sessionSaleBuilder.getItems();
        ModelAndView mv = new ModelAndView("sales/saleTableItem");
        mv.addObject("items", items);
        mv.addObject("totalValue", sessionSaleBuilder.getTotalValue());
        return mv;
    }

//    @PostMapping("/item")
//    public @ResponseBody String item(Long beerId) {
//        Beer one = beers.findOne(beerId);
//
//        sessionSaleBuilder.addItem(one, 1);
//
//        return "ok: " + sessionSaleBuilder.size();
//    }


    /**
     * The @InitBinder annotated methods will get called on each HTTP request if we don't specify the 'value' element of this annotation.
     * <p>
     * Each time this method is called a new instance of WebDataBinder is passed to it.
     * <p>
     * To be more specific about which objects our InitBinder method applies to,
     * we can supply 'value' element of the annotation @InitBinder.
     * The 'value' element is a single or multiple names of command/form attributes and/or request
     * parameters that this init-binder method is supposed to apply to.
     * <p>
     * for more details we can see the blog post: https://www.logicbig.com/tutorials/spring-framework/spring-web-mvc/spring-custom-property-editor.html
     *
     * @param binder the binder configuration object
     */
    @InitBinder("sale")
    public void configureBinder(WebDataBinder binder) {
        binder.initDirectFieldAccess();
        binder.setValidator(this.saleValidator);
    }

}
