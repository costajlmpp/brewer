package io.costax.brewer.controllers;

import io.costax.brewer.application.CityService;
import io.costax.brewer.application.Logger;
import io.costax.brewer.controllers.page.PageWrapper;
import io.costax.brewer.domain.model.client.City;
import io.costax.brewer.domain.repositories.client.Cities;
import io.costax.brewer.domain.repositories.client.Countries;
import io.costax.brewer.domain.repositories.client.filters.CityFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static io.costax.brewer.controllers.Messages.SUCCESS_MESSAGE;
import static io.costax.brewer.controllers.Views.CITIES_NEW;
import static io.costax.brewer.controllers.Views.CITIES_SEARCH;

@Controller
@RequestMapping("/cities")
public class CitiesController {

    private final Cities cities;
    private final Logger logger;
    private final Countries countries;
    private final CityService cityService;

    @Autowired
    public CitiesController(Cities cities, Logger logger, Countries countries, CityService cityService) {
        this.cities = cities;
        this.logger = logger;
        this.countries = countries;
        this.cityService = cityService;
    }

    @Cacheable(value = "cities", key = "#countryId") // must be the same name of the method parameter name
    @ResponseBody
    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<City> getCitiesByCountry(@RequestParam(name = "countryId", defaultValue = "-1") Long countryId) {
        return cities.findByCountryId(countryId);
    }

    @RequestMapping(value = "/new")
    public ModelAndView toCreateNew(City city) {
        // Spring is clever enough to know that if we are putting the client parameter in the method declaration
        // we should also want to have it in the template renderization.
        // if no client instance is present in the invocation then the spring will instantiate a new one.
        // So, we do not need to use the model implicitly to implement this behavior:
        // model.addAttribute(client));
        // Because Spring will make the invocation of this instruction through behind the scenes

        logger.info(() -> "go to /cities/new");

        ModelAndView mv = new ModelAndView(CITIES_NEW);

        mv.addObject("countries", countries.findAll());
        mv.addObject("city", city);

        return mv;
    }

    @Secured("ROLE_MANAGER_CITIES")
    // @CacheEvict(value = "cities", allEntries = true) // clean up all the cache
    @CacheEvict(value = "cities", key = "#city.country.id", condition = "#city.country != null", beforeInvocation = true)
    // invalidate only the cache of the country Id, the key value must be same name of the parameter method
    @PostMapping("/new")
    public ModelAndView save(@Valid City city, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return toCreateNew(city);
        }

        cityService.save(city);

        redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, "City added successful!!!");
        return new ModelAndView("redirect:/cities/new");
    }

    @GetMapping
    public ModelAndView search(CityFilter filter,
                               BindingResult result,
                               @PageableDefault(size = 10) Pageable pageable,
                               HttpServletRequest httpServletRequest) {

        final ModelAndView mv = new ModelAndView(CITIES_SEARCH);
        mv.addObject("filter", filter); // don't need to add this, but just for more simple understand
        mv.addObject("counties", countries.findAll());

        final PageWrapper<City> pageWrapper = new PageWrapper<>(cities.search(filter, pageable), httpServletRequest);

        mv.addObject("page", pageWrapper);
        return mv;
    }

}
