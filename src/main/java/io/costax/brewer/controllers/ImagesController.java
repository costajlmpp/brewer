package io.costax.brewer.controllers;


import io.costax.brewer.application.Logger;
import io.costax.brewer.application.storage.Storage;
import io.costax.brewer.application.storage.StoreImageRunnable;
import io.costax.brewer.domain.model.beer.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/images")
public class ImagesController {

    private final ExecutorService saveImagesTasks = Executors.newFixedThreadPool(5);
    private final Logger logger;
    private final Storage storage;

    @Autowired
    public ImagesController(Logger logger, Storage storage) {
        this.logger = logger;
        this.storage = storage;
    }

    @PostMapping
    public DeferredResult<Image> saveImage(@RequestParam("files[]") MultipartFile[] files) {
        DeferredResult<Image> result = new DeferredResult<>();

        saveImagesTasks.submit(new StoreImageRunnable(files, result, storage));

        return result;
    }

    @GetMapping("/temp/{name:.*}")
    public ResponseEntity<byte[]> getTemporaryImage(@PathVariable String name) {
        return storage.getTemporaryDoc(name)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/{name:.*}")
    public ResponseEntity<byte[]> getImage(@PathVariable String name) {
        return storage.getDoc(name)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
