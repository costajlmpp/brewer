package io.costax.brewer.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NavigationErrorController {

    @GetMapping("/404")
    public String resourceNotFound() {
        return "404";
    }

    @GetMapping("/500")
    public String applicationError() {
        return "500";
    }
}
