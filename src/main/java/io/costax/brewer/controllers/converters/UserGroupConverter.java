package io.costax.brewer.controllers.converters;

import io.costax.brewer.domain.model.profiles.UserGroup;
import org.springframework.core.convert.converter.Converter;

public class UserGroupConverter implements Converter<String, UserGroup> {

    @Override
    public UserGroup convert(String identifier) {
        if (identifier == null || identifier.trim().isEmpty()) {
            return null;
        }

        return UserGroup.withId(Long.valueOf(identifier));
    }
}
