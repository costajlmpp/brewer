package io.costax.brewer.controllers.converters;

import io.costax.brewer.domain.model.client.City;
import org.springframework.core.convert.converter.Converter;

public class CityConverter implements Converter<String, City> {

    @Override
    public City convert(String source) {
        try {
            Long styleId = Long.valueOf(source);
            return City.of(styleId);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
