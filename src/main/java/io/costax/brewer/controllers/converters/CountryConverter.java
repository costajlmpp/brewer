package io.costax.brewer.controllers.converters;

import io.costax.brewer.domain.model.client.Country;
import org.springframework.core.convert.converter.Converter;

public class CountryConverter implements Converter<String, Country> {

    @Override
    public Country convert(String source) {
        try {

            return Country.of(Long.valueOf(source));

        } catch (NumberFormatException e) {
            return null;
        }
    }
}
