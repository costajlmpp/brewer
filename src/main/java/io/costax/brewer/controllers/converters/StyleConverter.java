package io.costax.brewer.controllers.converters;

import io.costax.brewer.domain.model.beer.Style;
import org.springframework.core.convert.converter.Converter;

public class StyleConverter implements Converter<String, Style> {

    @Override
    public Style convert(String source) {
        try {
            Long styleId = Long.valueOf(source);
            return Style.of(styleId);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
