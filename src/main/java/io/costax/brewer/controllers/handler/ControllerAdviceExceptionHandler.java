package io.costax.brewer.controllers.handler;

import io.costax.brewer.domain.model.beer.StyleNameAlreadyRegisteredException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerAdviceExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<String> handlerStyleNameAlreadyRegisteredException(StyleNameAlreadyRegisteredException e) {
        return ResponseEntity.badRequest().body(e.getDetailMessage());
    }
}
