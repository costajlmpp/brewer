package io.costax.brewer.controllers;

public final class Views {

    /**
     * templates/beers/search-beers.html
     */
    public static final String BEER_SEARCH = "beers/search-beers";
    /**
     * templates/beers/new-beer.html
     */
    public static final String BEER_NEW = "beers/new-beer";
    /**
     * templates/styles/search-styles.html
     */
    public static final String STYLES_SEARCH = "styles/search-styles";
    /**
     * templates/styles/new-style.html
     */
    public static final String STYLES_NEW = "styles/new-style";
    /**
     * templates/clients/new-client.html
     */
    public static final String CLIENTS_NEW = "clients/new-client";
    /**
     * templates/clients/search-clients.html
     */
    public static final String CLIENTS_SEARCH = "clients/search-clients";

    /**
     * templates/cities/new-city.html
     */
    public static final String CITIES_NEW = "cities/new-city";

    /**
     * templates/cities/search-cities.html
     */
    public static final String CITIES_SEARCH = "cities/search-cities";

    /**
     * templates/users/new-user.html
     */
    public static final String USERS_NEW_USER = "users/new-user";
    public static final String USERS_SEARCH = "users/users";


    private Views() {
        throw new AssertionError();
    }

    /**
     * Put the prefix "redirect:" in the passing Path.
     * If the input has "/beers/new" then the output should be "redirect:/beers/new".
     */
    public static String redirectToRequestPath(String requestPath) {
        return "redirect:" + requestPath;
    }
}
