package io.costax.brewer.controllers.page;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.IntStream;

public class PageWrapper<T> {

    private final UriComponentsBuilder uriBuilder;
    private final Page<T> page;

    public PageWrapper(Page<T> page, HttpServletRequest httpServletRequest) {
        this.page = page;
        //this.uriBuilder = ServletUriComponentsBuilder.fromRequest(httpServletRequest);

        // this is a workaround that fix the query param String that contains spaces characters.
        // We should use the previous instructions instead
        // https://jira.spring.io/browse/SPR-10172
        final String replace = httpServletRequest.getRequestURL().append(
                httpServletRequest.getQueryString() != null ? "?" + httpServletRequest.getQueryString() : "")
                .toString().replace("+", "%20");
        this.uriBuilder = UriComponentsBuilder.fromHttpUrl(replace);
    }

    public List<T> getContent() {
        return page.getContent();
    }

    public int getTotalPages() {
        return page.getTotalPages();
    }

    public int getNumber() {
        return page.getNumber();
    }

    public int getSize() {
        return page.getSize();
    }

    public boolean isFirst() {
        return page.isFirst();
    }

    public boolean isLast() {
        return page.isLast();
    }

    public boolean isEmpty() {
        return this.getContent().isEmpty();
    }

    public String urlToPage(int page) {
        return uriBuilder.replaceQueryParam("page", page)
                .build(true)
                .encode()
                .toUriString();
    }

    public int[] getPaginationItems() {
        final int totalPages = page.getTotalPages();
        final int navMaxSize = 5;

        if (totalPages <= navMaxSize) {
            return IntStream.rangeClosed(0, totalPages == 0 ? 0 : totalPages - 1).toArray();
        }

        final int currentPage = this.getNumber();
        final int navAgvMaxSize = navMaxSize / 2;

        if (currentPage <= navAgvMaxSize) {
            return IntStream.range(0, navMaxSize).toArray();
        }

        if (currentPage >= totalPages - navAgvMaxSize - 1) {
            final int startAt = totalPages - navMaxSize;
            return IntStream.range(startAt, totalPages).toArray();
        }

        final int startAt = currentPage - navAgvMaxSize;
        final int endAt = currentPage + navAgvMaxSize;

        return IntStream.rangeClosed(startAt, endAt).toArray();
    }


    public String urlOrderBy(String field) {
        UriComponentsBuilder uriBuilderOrder = UriComponentsBuilder
                .fromUriString(uriBuilder.build(true).encode().toUriString());

        String sortQueryParam = String.format("%s,%s", field, invertOrder(field));

        return uriBuilderOrder.replaceQueryParam("sort", sortQueryParam).build(true)
                .encode().toUriString();
    }

    public String invertOrder(String field) {
        String direction = "asc";

        Sort.Order order = page.getSort() != null ? page.getSort().getOrderFor(field) : null;

        if (order != null) {
            direction = Sort.Direction.ASC.equals(order.getDirection()) ? "desc" : "asc";
        }

        return direction;
    }

    public boolean desc(String field) {
        return invertOrder(field).equals("asc");
    }

    public boolean order(String field) {
        Sort.Order order = page.getSort() != null ? page.getSort().getOrderFor(field) : null;

        if (order == null) {
            return false;
        }

        return page.getSort().getOrderFor(field) != null ? true : false;
    }

}
