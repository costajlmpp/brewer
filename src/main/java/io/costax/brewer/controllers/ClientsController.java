package io.costax.brewer.controllers;

import io.costax.brewer.application.ClientService;
import io.costax.brewer.controllers.page.PageWrapper;
import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.client.ClientSummary;
import io.costax.brewer.domain.model.client.NifAlreadyRegisteredException;
import io.costax.brewer.domain.repositories.Strings;
import io.costax.brewer.domain.repositories.client.Clients;
import io.costax.brewer.domain.repositories.client.Countries;
import io.costax.brewer.domain.repositories.client.filters.ClientFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static io.costax.brewer.controllers.Messages.SUCCESS_MESSAGE;

@Controller
public class ClientsController {

    private final Countries countries;
    private final ClientService clientService;
    private final Clients clients;

    @Autowired
    public ClientsController(Countries countries, ClientService clientService, Clients clients) {
        this.countries = countries;
        this.clientService = clientService;
        this.clients = clients;
    }

    @RequestMapping(value = "/clients/new")
    public ModelAndView toCreateNew(Client client) {
        // Spring is clever enough to know that if we are putting the client parameter in the method declaration
        // we should also want to have it in the template renderization.
        // if no client instance is present in the invocation then the spring will instantiate a new one.
        // So, we do not need to use the model implicitly to implement this behavior:
        // model.addAttribute(client));
        // Because Spring will make the invocation of this instruction through behind the scenes

        ModelAndView mv = new ModelAndView(Views.CLIENTS_NEW);

        mv.addObject("countries", countries.findAll());
        mv.addObject("client", client);

        return mv;
    }

    @PostMapping("/clients/new")
    public ModelAndView create(@Valid Client client,
                               BindingResult bindingResult,
                               RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return toCreateNew(client);
        }

        try {
            clientService.save(client);
        } catch (NifAlreadyRegisteredException e) {
            final String errorCode = e.getDetailMessage();
            bindingResult.rejectValue("nif", errorCode, errorCode);
            return toCreateNew(client);
        }

        redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, "Client added successful!!!");

        return new ModelAndView("redirect:/clients/new");
    }

    @GetMapping("/clients")
    public ModelAndView search(ClientFilter filter, BindingResult result,
                               @PageableDefault(size = 5) Pageable pageable,
                               HttpServletRequest httpServletRequest) {
        ModelAndView mv = new ModelAndView(Views.CLIENTS_SEARCH);

        mv.addObject("filter", filter);

        Page<Client> page = clients.search(filter, pageable);

        PageWrapper<Client> pageWrapper = new PageWrapper<>(page, httpServletRequest);

        mv.addObject("page", pageWrapper);

        return mv;
    }

    @GetMapping(value = "/clients/summary", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<ClientSummary> searchByName(String name) {
        if (Strings.isBlank(name) || name.length() < 3) {
            throw new IllegalArgumentException();
        }

        return this.clients.findByName(name);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Void> handlerIllegalArgumentException(IllegalArgumentException e) {
        return ResponseEntity.badRequest().build();
    }
}
