package io.costax.brewer.controllers;

import io.costax.brewer.application.Logger;
import io.costax.brewer.application.StyleService;
import io.costax.brewer.controllers.page.PageWrapper;
import io.costax.brewer.domain.model.beer.Style;
import io.costax.brewer.domain.model.beer.StyleNameAlreadyRegisteredException;
import io.costax.brewer.domain.repositories.beer.Styles;
import io.costax.brewer.domain.repositories.beer.filters.StyleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/styles")
public class StylesController {

    private final Styles styles;
    private final Logger logger;
    private final StyleService styleService;


    @Autowired
    public StylesController(StyleService styleService, Styles styles, Logger logger) {
        this.styleService = styleService;
        this.styles = styles;
        this.logger = logger;
    }

    @GetMapping
    public ModelAndView search(StyleFilter filter, @PageableDefault(size = 5) Pageable pageable, HttpServletRequest httpServletRequest) {
        ModelAndView mv = new ModelAndView(Views.STYLES_SEARCH);

        final Page<Style> page = styles.search(filter, pageable);
        PageWrapper<Style> pageWrapper = new PageWrapper<>(page, httpServletRequest);
        mv.addObject("page", pageWrapper);
        mv.addObject("filter", filter);

        return mv;
    }


    @RequestMapping("/new")
    public ModelAndView toNew(Style style) {
        logger.debug("navigation to new style view");

        final ModelAndView modelAndView = new ModelAndView(Views.STYLES_NEW);

        // according the documentation, this is not need it,
        // we just added this instruction in order to remove some IDE error
        modelAndView.addObject("style", style);

        return modelAndView;
    }


    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ModelAndView createNewStyle(@Valid Style style, BindingResult result, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            return toNew(style);
        }

        try {
            styleService.save(style);
        } catch (StyleNameAlreadyRegisteredException e) {
            final String errorCode = e.getDetailMessage();
            result.rejectValue("name", errorCode, errorCode);
            return toNew(style);
        }

        attributes.addFlashAttribute(Messages.SUCCESS_MESSAGE, "Style added successful!!!");
        return new ModelAndView("redirect:/styles/new");
    }

    /**
     * This a request like RESTFull.
     *
     * @RequestMapping(value = "/v1/styles", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
     * The @ResponseBody annotation tells a controller that the object returned is automatically serialized into JSON and passed back into the HttpResponse object.
     */
    @RequestMapping(method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<?> createNewStyle(@RequestBody @Valid Style style, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().body(result.getFieldError("name").getDefaultMessage());
        }

        try {
            style = styleService.save(style);
        } catch (StyleNameAlreadyRegisteredException e) {
            return ResponseEntity.badRequest().body(e.getDetailMessage());
        }

        return ResponseEntity.ok(style);
    }

    @InitBinder("style")
    public void configureBinder(WebDataBinder binder) {
        binder.initDirectFieldAccess();
    }
}
