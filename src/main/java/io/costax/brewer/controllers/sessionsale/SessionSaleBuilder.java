package io.costax.brewer.controllers.sessionsale;

import io.costax.brewer.domain.model.beer.Beer;
import io.costax.brewer.domain.model.client.Client;
import io.costax.brewer.domain.model.sale.Sale;
import io.costax.brewer.domain.model.sale.SaleItemSummary;
import io.costax.brewer.domain.model.sale.SalesItem;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@SessionScope
public class SessionSaleBuilder {

    private Sale sale;

    public SessionSaleBuilder addItem(Beer beer, int qty) {
        createSaleIfNotExists();

        this.sale.add(beer, qty);

        return this;
    }

    public int size() {
        createSaleIfNotExists();
        return sale.size();
    }

    public List<SaleItemSummary> getItems() {
        createSaleIfNotExists();
        return this.sale.getItems().stream()
                .map(SalesItem::toSummary)
                .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
    }

    public SessionSaleBuilder changeQty(final Beer beer, final Integer qty) {
        createSaleIfNotExists();
        this.sale.setQty(beer, qty);
        return this;
    }

    private void createSaleIfNotExists() {
        if (this.sale == null) {
            this.sale = Sale.empty();
        }
    }

    public SessionSaleBuilder remove(final Beer beer) {
        createSaleIfNotExists();
        this.sale.remove(beer);
        return this;
    }

    public BigDecimal getTotalValue() {
        createSaleIfNotExists();
        return this.sale.getTotal();
    }

    public Sale get() {
        createSaleIfNotExists();
        return this.sale;
    }

    public SessionSaleBuilder cleanup() {
        this.sale = null;
        return this;
    }

    public SessionSaleBuilder withClient(final Client of) {
        sale.setClient(of);
        return this;
    }

    public SessionSaleBuilder withPortsValue(final BigDecimal portsValue) {
        this.sale.setPortsValue(portsValue);
        return this;
    }

    public SessionSaleBuilder withDiscountValue(final BigDecimal discountValue) {
        this.sale.setDiscountValue(discountValue);
        return this;
    }
}
