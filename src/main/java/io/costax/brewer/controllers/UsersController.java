package io.costax.brewer.controllers;

import io.costax.brewer.application.Logger;
import io.costax.brewer.application.UserService;
import io.costax.brewer.controllers.page.PageWrapper;
import io.costax.brewer.domain.model.profiles.EmailAlreadyRegistedException;
import io.costax.brewer.domain.model.profiles.PasswordUndefinedException;
import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.model.profiles.UserGroup;
import io.costax.brewer.domain.repositories.profile.UserGroups;
import io.costax.brewer.domain.repositories.profile.Users;
import io.costax.brewer.domain.repositories.profile.filters.UserFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {

    private final Logger logger;
    private final UserGroups userGroups;
    private final Users users;

    private final UserService userService;

    @Autowired
    public UsersController(final Logger logger,
                           final Users users,
                           final UserGroups userGroups,
                           final UserService userService) {
        this.logger = logger;
        this.users = users;
        this.userGroups = userGroups;
        this.userService = userService;
    }

    /**
     * If we don't need to return nothing to the view then we can just return a string eg.
     * <pre>
     *
     *      @RequestMapping(value = "/users/new")
     *     public String ping(User user) {
     *
     * </pre>
     */
    @RequestMapping(value = "/new")
    public ModelAndView toNew(User user) {
        logger.debug("navigation to new user view");

        final ModelAndView modelAndView = new ModelAndView(Views.USERS_NEW_USER);

        // TODO: cache this list
        final List<UserGroup> allUserGroups = userGroups.findAll();

        // according the documentation, this is not need it,
        // we just added this instruction in order to remove some IDE error
        modelAndView.addObject("user", user);
        modelAndView.addObject("allUserGroups", allUserGroups);

        return modelAndView;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid User user, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return toNew(user);
        }

        try {
            this.userService.save(user);
        } catch (EmailAlreadyRegistedException e) {
            final String errorCode = e.getDetailMessage();
            result.rejectValue("email", errorCode, errorCode);
            return toNew(user);
        } catch (PasswordUndefinedException e) {
            result.rejectValue("password", "", "The Password should be defined");
            return toNew(user);
        } catch (Exception e) {
            final String errorCode = e.getMessage();
            //result.rejectValue("name", errorCode, errorCode);
            result.reject(null, e.getMessage());
            return toNew(user);
        }

        //redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, "Beer added successful!!!");
        redirectAttributes.addFlashAttribute(Messages.SUCCESS_MESSAGE, "User added successful!!!");
        return new ModelAndView("redirect:/users/new");
    }

    @GetMapping
    public ModelAndView search(UserFilter filter,
                               @PageableDefault(size = 3) Pageable pageable,
                               HttpServletRequest httpServletRequest) {
        ModelAndView mv = new ModelAndView(Views.USERS_SEARCH);

        mv.addObject("filter", filter);

        mv.addObject("groups", userGroups.findAll());
        PageWrapper<User> paginaWrapper = new PageWrapper<User>(users.search(filter, pageable), httpServletRequest);
        mv.addObject("page", paginaWrapper);

        return mv;
    }


    @PutMapping("/status")
    @ResponseStatus(HttpStatus.OK)
    public void updateStatus(@RequestParam("ids[]") Integer[] usersIds, @RequestParam("active") Boolean active) {
        userService.changeStatus(usersIds, active);
    }
}
