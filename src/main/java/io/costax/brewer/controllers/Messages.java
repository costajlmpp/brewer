package io.costax.brewer.controllers;

final class Messages {

    static final String SUCCESS_MESSAGE = "successMessage";

    private Messages() {
        throw new AssertionError("Messages class can't be instantiated");
    }
}
