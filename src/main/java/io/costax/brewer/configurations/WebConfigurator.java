package io.costax.brewer.configurations;

import com.github.mxab.thymeleaf.extras.dataattribute.dialect.DataAttributeDialect;
import com.google.common.cache.CacheBuilder;
import io.costax.brewer.application.Logger;
import io.costax.brewer.application.internal.LoggerBridge;
import io.costax.brewer.controllers.converters.CityConverter;
import io.costax.brewer.controllers.converters.CountryConverter;
import io.costax.brewer.controllers.converters.StyleConverter;
import io.costax.brewer.controllers.converters.UserGroupConverter;
import io.costax.brewer.controllers.thymeleaf.BrewerDialect;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.repository.support.DomainClassConverter;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.number.NumberStyleFormatter;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * This is a spring configurations class, it must be marked with the annotation @Configuration.
 * The Annotations @EnableSpringDataWebSupport give support to spring data in spring mvc like the pageable resource
 */
@Configuration
//@ComponentScan(basePackageClasses = {SomeClass.class})
@ComponentScan("io.costax.brewer.controllers")
@EnableWebMvc
@EnableSpringDataWebSupport
@EnableCaching
public class WebConfigurator extends WebMvcConfigurerAdapter implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Bean
    public ViewResolver viewResolver() {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        resolver.setCharacterEncoding("UTF-8");

        return resolver;
    }

    @Bean
    public TemplateEngine templateEngine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setEnableSpringELCompiler(true);
        engine.setTemplateResolver(templateResolver());

        // define dialect
        engine.addDialect(new LayoutDialect());
        engine.addDialect(new BrewerDialect());
        engine.addDialect(new DataAttributeDialect());
        engine.addDialect(new SpringSecurityDialect());
        engine.addDialect(new Java8TimeDialect());

        return engine;
    }

    @Bean
    public ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        resolver.setApplicationContext(applicationContext);
        resolver.setPrefix("classpath:/templates/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode(TemplateMode.HTML);

        return resolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //super.addResourceHandlers(registry);
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");
    }

    /**
     * This method registers all Custom Spring MVC Converters.
     * <p>
     * The name of the method and its signature must be exactly:
     * <pre>
     * public FormattingConversionService mvcConversionService()
     * </pre>
     * because spring will explicitly search the method with exactly that signature to find the Custom Converters
     * </p>
     */
    @Bean
    public FormattingConversionService mvcConversionService() {
        org.springframework.format.support.DefaultFormattingConversionService conversionService = new org.springframework.format.support.DefaultFormattingConversionService();
        conversionService.addConverter(new StyleConverter());
        conversionService.addConverter(new CityConverter());
        conversionService.addConverter(new CountryConverter());
        conversionService.addConverter(new UserGroupConverter());

        NumberStyleFormatter bigDecimalFormatter = new NumberStyleFormatter("#,##0.00");
        conversionService.addFormatterForFieldType(BigDecimal.class, bigDecimalFormatter);
        NumberStyleFormatter integerFormatter = new NumberStyleFormatter("#,##0");
        conversionService.addFormatterForFieldType(Integer.class, integerFormatter);

        // API Java 8 Date
        DateTimeFormatterRegistrar dateTimeFormatterRegistrar = new DateTimeFormatterRegistrar();
        //dateTimeFormatterRegistrar.setDateFormatter(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        dateTimeFormatterRegistrar.setDateFormatter(DateTimeFormatter.ISO_LOCAL_DATE);
        dateTimeFormatterRegistrar.registerFormatters(conversionService);

        return conversionService;
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public Logger logger(InjectionPoint injectionPoint) {
        final Class<?> containingClass = injectionPoint.getMethodParameter().getContainingClass();
        return LoggerBridge.of(LoggerFactory.getLogger(containingClass));
    }

//    @Bean
//    public org.springframework.web.servlet.LocaleResolver localResolver() {
//        Locale locale = new Locale("pt", "PT");
//        final FixedLocaleResolver fixedLocaleResolver = new FixedLocaleResolver(locale);
//
//        return fixedLocaleResolver;
//    }


    @Bean
    public CacheManager cacheManager() {
        //return new ConcurrentMapCacheManager();

        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder()
                .maximumSize(3)
                .expireAfterAccess(1, TimeUnit.MINUTES);

        GuavaCacheManager cacheManager = new GuavaCacheManager();
        cacheManager.setCacheBuilder(cacheBuilder);
        return cacheManager;
    }

    /**
     * Configure the file where ours custom messages are going to be defined.
     */
    @Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
        bundle.setBasename("classpath:/brewer-messages");
        bundle.setDefaultEncoding("UTF-8"); // http://www.utf8-chartable.de/
        return bundle;
    }

    @Bean
    public DomainClassConverter<FormattingConversionService> domainClassConverter() {
        return new DomainClassConverter<FormattingConversionService>(mvcConversionService());
    }
}
