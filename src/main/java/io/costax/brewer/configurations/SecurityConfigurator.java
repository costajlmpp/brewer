package io.costax.brewer.configurations;


import io.costax.brewer.security.AppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@ComponentScan(basePackageClasses = AppUserDetailsService.class)
public class SecurityConfigurator extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // Use just for testing
        /*auth.inMemoryAuthentication()
                .withUser("admin")
                .password("admin")
                .roles("CREATE_CLIENT");
                */

        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());

    }

    @Override
    public void configure(final WebSecurity web) {
        web.ignoring()
                .antMatchers("/layout/**")
                .antMatchers("**/images/**")
                .antMatchers("/sw.js");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http.authorizeRequests()
                    //.antMatchers("/layout/**").permitAll()
                    //.antMatchers("/layout/images/**").permitAll()
                    .antMatchers("/cities/new").hasRole("MANAGER_CITIES")
                    .antMatchers("/users/**").hasRole("MANAGER_USERS")
                    .anyRequest().authenticated()
                    //.anyRequest().denyAll()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                .and()
                    .logout()
				        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .and()
                    .exceptionHandling()
				        .accessDeniedPage("/403")
                // uncomment just in case your application do not contain CSRF configured
                //.and().csrf().disable()

                // Session configurations
                .and()
                    .sessionManagement()
                            .maximumSessions(1)
                            .expiredUrl("/login")
                        .and()
                            .invalidSessionUrl("/login")

                ;
        //@formatter:on
    }
}
