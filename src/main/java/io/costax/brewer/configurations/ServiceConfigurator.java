package io.costax.brewer.configurations;

import io.costax.brewer.application.BeerService;
import io.costax.brewer.application.Logger;
import io.costax.brewer.application.internal.LocalStorage;
import io.costax.brewer.application.internal.LoggerBridge;
import io.costax.brewer.application.storage.Storage;
import io.costax.brewer.security.AppUserDetailsService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(basePackageClasses = {BeerService.class, AppUserDetailsService.class})
public class ServiceConfigurator {

    @Bean
    public Storage localStorage() {
        return new LocalStorage();
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public Logger logger(InjectionPoint injectionPoint) {
        final Class<?> containingClass = injectionPoint.getMethodParameter().getContainingClass();
        return LoggerBridge.of(LoggerFactory.getLogger(containingClass));
    }
}
