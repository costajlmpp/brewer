package io.costax.brewer.configurations.init;

import io.costax.brewer.configurations.JPAConfigurator;
import io.costax.brewer.configurations.SecurityConfigurator;
import io.costax.brewer.configurations.ServiceConfigurator;
import io.costax.brewer.configurations.WebConfigurator;
import org.springframework.web.filter.HttpPutFormContentFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                JPAConfigurator.class,
                ServiceConfigurator.class,
                SecurityConfigurator.class
        };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebConfigurator.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        // which urls will be delivered to the servlet dispatcher
        return new String[]{
                "/"
        };
    }

    @Override
    protected Filter[] getServletFilters() {
        // try to add a CharacterEncodingFilter if we have some problem with accentuation
        // new CharacterEncodingFilter("UTF-8", true)

        // by default the spring do not allow HTTP PUT with form data,
        // to enable we must enable the filter
        HttpPutFormContentFilter httpPutFormContentFilter = new HttpPutFormContentFilter();

        return new Filter[]{
                httpPutFormContentFilter
        };
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        String targetFolderLocation = "";
        registration.setMultipartConfig(new MultipartConfigElement(targetFolderLocation));
    }
}
