package io.costax.brewer.security;

import com.google.common.base.Preconditions;
import io.costax.brewer.domain.model.profiles.User;
import io.costax.brewer.domain.repositories.profile.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AppUserDetailsService implements UserDetailsService {

    private static final String ROLE = "ROLE_";
    private final Users users;

    @Autowired
    public AppUserDetailsService(final Users users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        Preconditions.checkArgument(email != null, "The email is mandatory to find the user, it should not be null");

        final User user = users.findUserByEmailIgnoreCaseAndEnable(email)
                .orElseThrow(() -> new UsernameNotFoundException("invalid email or password"));

        return AppUserDetails.of(user);
    }
}
