package io.costax.brewer.security;

import io.costax.brewer.domain.model.profiles.Permission;
import io.costax.brewer.domain.model.profiles.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

public class AppUserDetails extends org.springframework.security.core.userdetails.User {

    private static final String ROLE = "ROLE_";

    private final User user;

    private AppUserDetails(User user, final Collection<? extends GrantedAuthority> grantedAuthorities) {
        super(user.getEmail(), user.getPassword(), grantedAuthorities);
        this.user = user;
    }

    private static Collection<? extends GrantedAuthority> grantedAuthorityOf(User u) {
        return u.getGroups().stream()
                .flatMap(g -> g.getPermissions().stream())
                .map(Permission::getName)
                .map(String::toUpperCase)
                .map(String::trim)
                .filter(s -> s.length() > 0)
                .map(r -> ROLE + r)
                .distinct()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));
    }

    static AppUserDetails of(User user) {
        Objects.requireNonNull(user);

        final Collection<? extends GrantedAuthority> grantedAuthorities = grantedAuthorityOf(user);

        return new AppUserDetails(user, grantedAuthorities);
    }

    public String getDisplayName() {
        return String.format("%s", this.user);
    }

    public User getUser() {
        return user;
    }
}

