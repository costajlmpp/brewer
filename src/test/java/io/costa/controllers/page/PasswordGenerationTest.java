package io.costa.controllers.page;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordGenerationTest {

    @Test
    public void passwordGeneration() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        final String password = encoder.encode("admin");


        System.out.println(password);

        //Assert.assertEquals("$2a$10$4ptzWDtg0QyquyPEISLHo.CwlHbjMYJrg9jyn2CtaV3dTCogJTrY6", password);
    }
}
