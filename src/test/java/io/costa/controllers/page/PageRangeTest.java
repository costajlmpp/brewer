package io.costa.controllers.page;

import org.junit.Test;

import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PageRangeTest {


    @Test
    public void rangeOfListOf5Pages() {
        final int[] range = range(0, 5);
        assertThat(range.length, is(5));
        assertThat(range[0], is(1));
        assertThat(range[4], is(5));
    }

    @Test
    public void rangeSecondPage() {
        final int[] range = range(1, 5);

        assertThat(range.length, is(5));
        assertThat(range[0], is(1));
        assertThat(range[4], is(5));
    }

    @Test
    public void rangeSixPageX() {
        final int[] range = range(6, 10);

        assertThat(range.length, is(5));
        assertThat(range[0], is(4));
        assertThat(range[4], is(8));
    }


    @Test
    public void rangeNinePageX() {
        final int[] range = range(9, 10);

        assertThat(range.length, is(5));
        assertThat(range[0], is(6));
        assertThat(range[4], is(10));
    }

    @Test
    public void rangeTenOfTenPages() {
        final int[] range = range(10, 10);

        assertThat(range.length, is(5));
        assertThat(range[0], is(6));
        assertThat(range[4], is(10));
    }



    int[] range(int currentPage, int totalPages) {


        int s = currentPage - 2;
        if (s <= 0) {
            s = 1;
        }

        if ( (s + 5) > totalPages ) {
            s = totalPages - 4;
        }

        return IntStream.rangeClosed(s, totalPages)
                .limit(5)
                //.skip(5)
                .toArray();
    }
}
