package io.costax.brewer.domain.model.sale;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SalesItemPkTest {

    @Test
    public void testTransientState() {
        SalesItemPk one = SalesItemPk.of(null, 1L);
        SalesItemPk two = SalesItemPk.of(null, 1L);

        Assert.assertThat(one, Matchers.equalTo(two));
        assertEquals(one.hashCode(), two.hashCode());
    }

    @Test
    public void testPersistedState() {
        SalesItemPk one = SalesItemPk.of(1L, 2L);
        SalesItemPk two = SalesItemPk.of(1L, 2L);

        Assert.assertThat(one, Matchers.equalTo(two));
        assertEquals(one.hashCode(), two.hashCode());
    }
}