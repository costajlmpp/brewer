package io.costax.brewer.domain.model.sale;

import io.costax.brewer.domain.model.client.Client;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static io.costax.brewer.domain.model.sale.SomeTestBeers.*;

public class SaleTest {

    Client client = new Client();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCreateAEmptySale() {
        Sale sale = createEmptySale();
    }

    @Test
    public void shouldNotAddLessOrEqualsToZeroQtyBeer() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("The qty must be greater than \"ZERO\"");

        Sale sale = createEmptySale();
        sale.add(A1, 0);
    }

    @Test
    public void shouldNotAddNullBeers() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can't add a null beer to a sale");

        Sale sale = createEmptySale();
        sale.add(null, 1);
    }

    private Sale createEmptySale() {
        return Sale.empty(client, null);
    }

    @Test
    public void shouldNotAddMultipleBeers() {
        Sale sale = createEmptySale();

        sale.add(A1, 1);
        sale.add(A2, 2);
        sale.add(A3, 1);
        sale.add(A1, 3);

        Assert.assertThat(sale.size(), Matchers.is(3));
        Assert.assertThat(sale.getItems(), Matchers.hasSize(3));
        BigDecimal expectedTotal = new BigDecimal("19.8");
        Assert.assertThat(sale.getTotal().compareTo(expectedTotal), Matchers.is(0));
    }

    @Test
    public void shouldDefineQtyOfItem() {
        Sale sale = createEmptySale();

        sale.add(A1, 1);
        sale.add(A2, 2);
        sale.add(A3, 1);
        sale.add(A1, 3);

        sale.setQty(A1, 1);

        Assert.assertThat(sale.size(), Matchers.is(3));
        Assert.assertThat(sale.getItems(), Matchers.hasSize(3));
        BigDecimal expectedTotal = new BigDecimal("13.5");
        Assert.assertThat(sale.getTotal().compareTo(expectedTotal), Matchers.is(0));
    }

    @Test
    public void shouldRemoveItemWhenDefineZeroQtyOfItem() {
        Sale sale = createEmptySale();

        sale.add(A1, 1);
        sale.add(A2, 2);
        sale.add(A3, 1);
        sale.add(A1, 3);

        sale.setQty(A1, 0);

        Assert.assertThat(sale.size(), Matchers.is(2));
        Assert.assertThat(sale.getItems(), Matchers.hasSize(2));
        BigDecimal expectedTotal = new BigDecimal("11.4");
        Assert.assertThat(sale.getTotal().compareTo(expectedTotal), Matchers.is(0));
        Assert.assertFalse(sale.containsBeer(A1));
        Assert.assertTrue(sale.containsBeer(A2));
        Assert.assertTrue(sale.containsBeer(A3));
    }

    @Test
    public void shouldNotDefineQtyOfUnExistingItem() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can't define the qty of nonexistent beer in a sale. The sale do not contain the beer");

        Sale sale = createEmptySale();

        sale.add(A1, 1);
        sale.add(A2, 2);
        sale.add(A3, 1);
        sale.add(A1, 3);

        sale.setQty(A4, 0);
    }

    @Test
    public void shouldRemoveUsingBeer() {
        Sale sale = createEmptySale();
        sale.add(A1, 1);
        sale.add(A1, 3);

        sale.add(A2, 2);
        sale.add(A3, 1);

        sale.remove(A1);

        Assert.assertThat(sale.size(), Matchers.is(2));
        Assert.assertThat(sale.getItems(), Matchers.hasSize(2));
        BigDecimal expectedTotal = new BigDecimal("11.4");
        Assert.assertThat(sale.getTotal().compareTo(expectedTotal), Matchers.is(0));
        Assert.assertFalse(sale.containsBeer(A1));
        Assert.assertTrue(sale.containsBeer(A2));
        Assert.assertTrue(sale.containsBeer(A3));
    }

}