package io.costax.brewer.domain.model.sale;

import io.costax.brewer.domain.model.client.Client;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;

public class SalesItemTest {

    Client client = new Client();

    @Test
    public void removeOnCollection() {
        final Sale sale = Sale.empty();

        Set<SalesItem> items = new HashSet<>();

        final SalesItem a = SalesItem.of(1, SomeTestBeers.A1, sale);
        final SalesItem b = SalesItem.of(1, SomeTestBeers.A2, sale);
        final SalesItem c = SalesItem.of(1, SomeTestBeers.A3, sale);
        items.add(a);
        items.add(b);
        items.add(c);

        Assert.assertThat(items, Matchers.hasSize(3));

        boolean remove = items.remove(b);

        Assert.assertThat(items, Matchers.hasSize(2));
    }

    @Test
    public void testTransientState() {
        Sale empty = Sale.empty();

        SalesItem one = SalesItem.of(1, SomeTestBeers.A1, empty);
        SalesItem two = SalesItem.of(2, SomeTestBeers.A1, empty);
        SalesItem three = SalesItem.of(1, SomeTestBeers.A3, empty);


        Assert.assertThat(one, equalTo(two));
        assertEquals(one.hashCode(), two.hashCode());

        Assert.assertThat(one, not(equalTo(three)));
        assertEquals(one.hashCode(), two.hashCode());
    }

    @Test
    public void testPersistedState() {
        SalesItemPk one = SalesItemPk.of(1L, 2L);
        SalesItemPk two = SalesItemPk.of(1L, 2L);

        Assert.assertThat(one, equalTo(two));
        assertEquals(one.hashCode(), two.hashCode());
    }

}