package io.costax.brewer.domain.model.sale;

import io.costax.brewer.domain.model.beer.Beer;

import java.math.BigDecimal;

public final class SomeTestBeers {

    static final Beer A1 = Beer.of("AA0001", "AA0001", BigDecimal.valueOf(2.1D));
    static final Beer A2 = Beer.of("AA0002", "AA0002", BigDecimal.valueOf(3.2D));
    static final Beer A3 = Beer.of("AA0003", "AA0003", BigDecimal.valueOf(5D));
    static final Beer A4 = Beer.of("AA0004", "AA0004", BigDecimal.valueOf(1.0D));
    static final Beer A5 = Beer.of("AA0005", "AA0005", BigDecimal.valueOf(2.99D));

}
