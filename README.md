# Brewer

## Setting up tomcat server

1. Download the Tomcat 8 in the the url: 
2. Download the mysql jdbc driver - mysql-connector-java-8.0.12.tar
3. Download c3p0-0.9.5.2.bin.zip
4. copy the next jars to the tomcat/lib folder
    - c3p0-0.9.5.2.jar
    - mchange-commons-java-0.2.11.jar
    - mysql-connector-java-8.0.12.jar


```bash

curl -o apache-tomcat-8.5.40.tar.gz http://mirrors.up.pt/pub/apache/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz

tar -xzf apache-tomcat-8.5.40.tar.gz

curl -o apache-tomcat-8.5.40/lib/mysql-connector-java-8.0.12.jar https://search.maven.org/classic/remotecontent?filepath=mysql/mysql-connector-java/8.0.12/mysql-connector-java-8.0.12.jar

curl -o apache-tomcat-8.5.40/lib/mchange-commons-java-0.2.11.jar https://search.maven.org/classic/remotecontent?filepath=com/mchange/mchange-commons-java/0.2.11/mchange-commons-java-0.2.11.jar

curl -o apache-tomcat-8.5.40/lib/c3p0-0.9.5.2.jar http://central.maven.org/maven2/com/mchange/c3p0/0.9.5.2/c3p0-0.9.5.2.jar


docker run --name brewer-mysql \
 -v $(pwd)/brewer-mysql:/var/lib/mysql \
 -p 3306:3306 \
 -e MYSQL_DATABASE=brewerdb \
 -e MYSQL_ROOT_PASSWORD=my-password \
 -d mysql:8.0.16
 
mkdir $(pwd)/brewer-images
export HOME=$(pwd)/brewer-images
 
cd brewer

mvn flyway:migrate -Dflyway.configFiles=flyway.properties && mvn clean package 

```

### Create the database

``` bash
$ docker run --name brewer-mysql \
 -v /Users/costa/Documents/junk/docker-volumes/brewer-mysql:/var/lib/mysql \
 -p 3306:3306 \
 -e MYSQL_DATABASE=brewerdb \
 -e MYSQL_ROOT_PASSWORD=my-password \
 -d mysql:8.0.12
```


### Environment variables

HOME=/Users/costa/Documents/junk/repo

### run flyway migrations

*Migration*

``` bash
mvn flyway:migrate \
    -Dflyway.baselineOnMigrate=true \
    -Dflyway.user=root \
    -Dflyway.password=my-password \
    -Dflyway.url=jdbc:mysql://localhost:3306/brewerdb 
```

*Clean up*

``` bash
mvn flyway:clean \
    -Dflyway.user=root \
    -Dflyway.password=my-password \
    -Dflyway.url=jdbc:mysql://localhost:3306/brewerdb 
```

*We can also run the previous commands using a external configuration file*

```
mvn flyway:migrate -Dflyway.configFiles=flyway.properties
```

# Enviroment variables

* HOME: path for the images volume, eg. /Users/costa/Documents/junk/repo


## dependencias terceiras

### Web

* bootstrap datepicker: https://bootstrap-datepicker.readthedocs.io/en/1.3.0-rc.4/options.html#startdate
   
   this is use to implements input datapicker    
   
* bootstrap-switch - Turn checkboxes and radio buttons into toggle switches.
    https://bttstrp.github.io/bootstrap-switch
    

### back