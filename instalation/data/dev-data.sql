INSERT INTO style (id, name) VALUES (1, 'Pilsener');
INSERT INTO style (id, name) VALUES (2, 'Dunkel');
INSERT INTO style (id, name) VALUES (3, 'Ambar');
INSERT INTO style (id, name) VALUES (4, 'Weiss');


INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (31, 'AA1234', 'A', 'A', 1.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'HOMEMADE', 1, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (32, 'AX1111', 's', 's', 1.00, 15.00, 0.10, 'TOASTED_BARLEY_AROMA', 'HOMEMADE', 3, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (33, 'AA1112', 'A', 's', 1.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'HOMEMADE', 1, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (34, 'AA3333', '3333', '3333', 1.00, 15.00, 0.10, 'TOASTED_BARLEY_AROMA', 'HOMEMADE', 2, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (35, 'AA4444', '44', '44', 1.00, 15.00, 0.10, 'TOASTED_BARLEY_AROMA', 'MANUFACTURED', 2, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (36, 'BA1000', 'BA1000', 'BA1000', 1.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'HOMEMADE', 2, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (37, 'BA1001', 'BA1001', 'BA1000', 3.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'MANUFACTURED', 2, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (38, 'BA1002', 'BA1002', 'sddd', 6.00, 15.00, 0.10, 'TOASTED_BARLEY_AROMA', 'MANUFACTURED', 4, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (39, 'CC0005', 'Simple Xine', 'Simple Xine', 8.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'HOMEMADE', 3, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (40, 'AB1278', 'Ola 123', 'sss', 1.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'HOMEMADE', 1, null, null);
INSERT INTO beer (id, code, name, description, value, alcoholic_strength, commission, flavor, origin, style_id, image_name, image_content_type) VALUES (41, 'AU7892', 'sasa', 'ssss', 1.00, 15.00, 0.10, 'SOFT_HOPPY_AROMA', 'MANUFACTURED', 1, null, null);


INSERT INTO country (id, name, acronym) VALUES (1, 'Portugal', 'PT');
INSERT INTO country (id, name, acronym) VALUES (2, 'Spain', 'ES');


